# Subtracker-rs

![tutti i README fatti bene hanno uno screenshot](res/screenshot.png)

## Introduzione

`subtracker-rs` è una reimplementazione di [subtracker](https://gitlab.com/subottosns/subtracker), in Rust.
In quanto tale, il suo lavoro è quello di analizzare in tempo reale il video di una webcam
che inquadra il subotto (o un qualunque altro biliardino) surante la [24h](https://24ore.uz.sns.it),
trovare la posizione della pallina e delle stecche,
e passarle al sito della 24h in modo che questo possa riprodurre la partita nella homepage.

Al momento `subtracker-rs` è stato compilato e testato solo su linux, ma in teoria nulla impedirebbe
di farlo funzionare anche su windows o macos, se mai qualcuno volesse provarci.

Le principali motivazioni per questa riscrittura sono:
- compilare il vecchio `subtracker` è un'impresa, `subtracker-rs` dovrebbe essere molto più semplice
- scrivere un programma di questo genere sembrava divertente

## Download
Se si vuole provare `subtracker-rs` e ci si sente fortunati, si può scaricare ed eseguire l'ultima
[AppImage](https://gitlab.com/giacomogallina/subtracker-rs/-/jobs/artifacts/master/raw/subtracker-rs-x86_64.AppImage?job=release)
([AppImage](https://appimage.org/) è un modo figo per produrre programmi per linux, indipendenti dalla distro
e dalle librerie installate)

L'AppImage viene compilata da gitlab ad ogni commit, vedi [build/.gitlab-ci.yml](build/.gitlab-ci.yml) e CI/CD > Jobs

## Compilazione
### Compilazione "standard"
Per compilare `subtracker-rs` servono queste dependencies:
- cose di base per lo sviluppo ([arch: base-devel](https://archlinux.org/groups/x86_64/base-devel/), [ubuntu: build-essential](https://packages.ubuntu.com/groovy/build-essential))
- opencv4 ([arch: opencv](https://www.archlinux.org/packages/extra/x86_64/opencv/),
[ubuntu: libopencv-dev](https://packages.ubuntu.com/groovy/libopencv-dev))
- tool per compilare rust (serve una versione decentemente recente di rustc, per sicurezza meglio l'ultima) ([arch](https://wiki.archlinux.org/index.php/Rust#Installation), [script ufficiale](https://www.rust-lang.org/tools/install))
- pacchetti a caso per la libreria della GUI, che quasi sicuramente sono preinstallati (ubuntu:
    [libxcb-xfixes0-dev](https://packages.ubuntu.com/hirsute/libxcb-xfixes0-dev),
    [libxkbcommon-dev](https://packages.ubuntu.com/hirsute/libxkbcommon-dev))
- clang ([arch: clang](https://archlinux.org/packages/extra/x86_64/clang/), [ubuntu: libclang-dev](https://packages.ubuntu.com/groovy/libclang-dev))
- llvm (forse non serve?) ([arch: llvm](https://archlinux.org/packages/extra/x86_64/llvm/), [ubuntu: llvm](https://packages.ubuntu.com/groovy/llvm))

Una volta installate queste dependencies, bisogna clonare il repo, entrare nella cartella del repo, ed eseguire

`cargo build --release`

Impiegherà un po' a compilare, e una volta finito l'eseguibile sarà

`./target/release/subtracker-rs`

Può essere utile guardare il [Dockerfile](build/Dockerfile) per vedere una procedura di compilazione che _dovrebbe_ funzionare

### AppImage tramite Docker
Se non si vuole/riesce a compilare nel modo standard, si può creare una AppImage
di `subtracker-rs` tramite un container docker. In questo modo tutto il necessario
per la compilazione viene scaricato dentro al container, dove tutto _dovrebbe_ filare
liscio, e alla fine dovrebbe sputare fuori `subtracker-rs` compilato e pronto per
essere eseguito su un pc qualsiasi. Serve ovviamente che sia installato
`docker`, e a quel punto basta eseguire

`./build/build.sh`

Lo script va eseguito da un utente che ha i permesi di usare docker (ad esempio 
`root`), e bisogna avere almeno 4.5 GB di disco liberi.
Una volta finita la compilazione si può eliminare l'immagine docker, eseguendo

`docker rmi subtracker-rs-build`

Se tutto va a buon fine, si dovrebbe ottenere una AppImage di `subtracker-rs`:

`./subtracker-rs-x86_64.AppImage`

La AppImage dovrebbe (si spera) contenere tutte le librerie necessarie per far
girare `subtracker-rs`, indipendentemente dalla distro usata o dai pacchetti installati,
e dovrebbe quindi essere completamente portable.

Questa è la stessa procedura che gitlab esegue per compilare l'AppImage ad ogni commit

## Documentazione
Per compilare la documentazione riguardante `subtracker-rs` e tutte le librerie
da cui dipende, basta eseguire (all'interno del repo)

`cargo doc`.

Per aprirla basta eseguire

`cargo doc --open`

## Utilizzo

Durante una 24h ci sono parecchi computer che fanno cose, in particolare sia lo streaming su YouTube che `subtracker` vogliono la webcam.
Visto che non vogliamo far bruciare i computer, queste due cose è bene metterle su computer diversi, e per questioni di importanza,
è meglio attaccare la webcam al computer di YouTube. `subtracker` deve quindi farsi mandare il video in qualche modo via rete.
Un modo "standard" di farlo è tramite il protocollo [RTSP](https://en.wikipedia.org/wiki/Real_Time_Streaming_Protocol), che usa questa struttura:

`video source -----> RTSP server -----> video consumer`

Serve quindi un server RTSP (ad esempio [rtsp-simple-server](https://github.com/aler9/rtsp-simple-server)), e qualcuno che gli mandi un video,
in questo caso `video_source.sh` (che altro non è che ffmpeg con qualche flag).

Per fare qualche prova in locale si può usare (a patto di avere un file video chiamato `sample.mp4`)

`./video_source.sh`

mentre durante una 24h vera e propria si può usare

`./video_source.sh /dev/videoX`

sul computer attaccato alla webcam. In entrambi i casi `subtracker` va eseguito con

`cargo run --release`

(o con l'AppImage)

Per collegare `subtracker` al server RTSP basta inserire il suo indirizzo nell'apposito campo e premere "connect".

`subtracker`, però, usa OpenCV (che a sua volta usa FFmpeg) per leggere il video in input, e FFmpeg supporta
[*tanti*](https://ffmpeg.org/ffmpeg-all.html#Protocols) protocolli per leggere video:
in particolare, si può selezionare la modalità "manual" e inserire un qualsiasi url supportato, e `subtracker` lo userà come
input video. Ad esempio, in questo modo si può usare una registrazione di una 24h vecchia per fare qualche prova
(`path/to/registrazione` o `http://sito/registrazione`), senza dover stare a scaricare server RTSP o cose simili
(però la velocità di riproduzione sarà abbastanza casuale), oppure usare in modo diretto una webcam (`/dev/videoX`),
o molto altro!


## Funzionalità Presenti
`subtracker-rs` riesce a ricevere via RTSP un input video, decodificarlo, trovare la posizione del tavolo con un
errore dell'ordine del paio di pixel, filtrare via lo sfondo del campo, 
trovare la pallina in situazioni ragionevoli, trovare la posizione e la rotazione delle stecche,
riordinare i risultati dei vari fotogrammi, e stamparli su stdout in un formato
compatibile con quello di `subtracker` originale.

## Funzionalità Mancanti
Sarebbe carino se riuscisse da solo a capire da che lato stanno i rossi e i blu,
e riuscisse a girare di conseguenza il video.

Inoltre, `subtracker-rs` fa un po' schifo come nome, è lungo, impronunciabile e
poco fantasioso, sarebbe bello un nome alternativo
