#!/bin/bash

for lib in $(ldd ./target/release/subtracker-rs | grep -o "\/[^ ]*")
do
		echo $lib
		usr_lib=$(echo $lib | sed "s/^\/lib\//\/usr\/lib\//g")
		if [ ! -d ./appdir$(dirname $usr_lib) ]; then
				mkdir -p ./appdir$(dirname $usr_lib)
		fi
		cp $lib ./appdir$(dirname $usr_lib)
done

cp /usr/lib/x86_64-linux-gnu/libX11.so.6 ./appdir/usr/lib/x86_64-linux-gnu/
