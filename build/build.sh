#!/bin/bash

# go to the root of the project
cd $(dirname $0)
cd ..
pwd

docker build -f build/Dockerfile --network=host -t subtracker-rs-build .
docker create --name subtracker-rs-temp subtracker-rs-build
docker cp subtracker-rs-temp:/usr/src/subtracker-rs/subtracker-rs-x86_64.AppImage .
docker rm -f subtracker-rs-temp

echo
echo finished in $(echo $SECONDS / 60 | bc)m $(echo $SECONDS % 60 | bc)s
