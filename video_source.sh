#!/bin/bash

if [ -z ${1+x} ]; then
    ffmpeg -re -stream_loop -1 -i sample.mp4 -an -c:v libx264 -x264-params keyint=20 -preset veryfast -f rtsp rtsp://0.0.0.0:8554/subtracker
else
    ffmpeg -i $1 -an -c:v libx264 -x264-params keyint=20 -preset veryfast -f rtsp rtsp://0.0.0.0:8554/subtracker
fi
