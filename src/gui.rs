//! For the GUI we use the [`eframe`] library, which uses [`egui`].
//!
//! [`egui`] is an immediate mode gui library, which means that
//! writing the gui is super simple, but you can't have complicated layouts.
//! For more information check the library docs directly
//!
//! For error popups and file selection dialogs, we use [`native_dialog`]

use std::fs::{File, OpenOptions};
use std::io::prelude::*;

use eframe::{self, egui};

use crate::video_processing::{config, Feedback, VideoProcessor, VideoProcessorHandle};

/// Various widgets that get repeated a lot
mod widgets;
use widgets::{color_chooser, toggle, LabeledDragValue, VideoPlayer};

use self::widgets::PipelineInfoPlot;

/// shows a [`native_dialog`] popup with the specified title and contents
fn error_popup(title: &str, err: &str) {
    if let Err(err2) = native_dialog::MessageDialog::new()
        .set_title(title)
        .set_text(err)
        .set_type(native_dialog::MessageType::Error)
        .show_alert()
    {
        eprintln!(
            "While trying to show this error popup:\n  {}\nAnother error occurred:\n  {}",
            err, err2
        );
    }
}

/// The state of the GUI
pub struct Gui {
    video_processor_handle: Option<VideoProcessorHandle>,
    config: config::Config,
    old_config: config::Config,
    feedback: Feedback,
    old_feedback: Feedback,
    video_players: Vec<VideoPlayer>,
    pipeline_info_plot: PipelineInfoPlot,
}

impl Gui {
    pub fn new() -> Self {
        Gui {
            video_processor_handle: None,
            config: config::Config::default(),
            old_config: config::Config::default(),
            feedback: Feedback::default(),
            old_feedback: Feedback::default(),
            video_players: (0..10).map(|_| VideoPlayer::new()).collect(),
            pipeline_info_plot: PipelineInfoPlot::new(),
        }
    }

    pub fn run(self) {
        // the appimage needs a special $LD_LIBRARY_PATH, that interferes with native_dialogs
        std::env::remove_var("LD_LIBRARY_PATH");
        // egui doesn't work very well on wayland, it's best to force xwayland for now
        std::env::remove_var("WAYLAND_DISPLAY");
        // opencv calls ffmpeg for the video decoding, and ffmpeg needs these options
        // https://answers.opencv.org/question/192178/how-to-set-ffmpeg-option-protocol_whitelist-fileudprtp-in-videocapture/
        std::env::set_var(
            "OPENCV_FFMPEG_CAPTURE_OPTIONS",
            "protocol_whitelist;file,rtp,udp,tcp",
        );
        // use tcp for better reliability
        std::env::set_var("OPENCV_FFMPEG_CAPTURE_OPTIONS", "rtsp_transport;tcp");

        let _ = eframe::run_native(
            "Subtracker",
            eframe::NativeOptions::default(),
            Box::new(|_| Box::new(self)),
        );
    }
}

impl eframe::App for Gui {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        ctx.set_visuals(egui::style::Visuals::dark());
        // ctx.set_style(Arc::new(
        // egui_aesthetix::themes::StandardDark::custom_style(),
        // ));
        egui::TopBottomPanel::bottom("Plot")
            .resizable(true)
            .default_height(200.0)
            .show(ctx, |ui| {
                self.pipeline_info_plot
                    .show(ui, &mut self.feedback.pipeline_info);
            });
        egui::SidePanel::left("Feedbacks").default_width(300.0).show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                ui.add_space(5.0);
                ui.heading("Feedbacks");
            });
            egui::ScrollArea::vertical().show(ui, |ui| {
                for ((name, feed, description), vp) in std::iter::IntoIterator::into_iter([
                    ("Original Video", &mut self.feedback.original, None),
                    ("Canny & Hough", &mut self.feedback.canny_and_hough, Some("Edges should be easily visible and rods should be covered with red lines")),
                    ("Rotated Original", &mut self.feedback.rotated, Some("Table should be straight and the red defense should be on the left")),
                    ("Rotated Green Mask", &mut self.feedback.rotated_green, Some("The field should be all white (even the sides and the corners)")),
                    ("Cropped Table", &mut self.feedback.cropped, None),
                    ("Color Masks", &mut self.feedback.colors, None),
                    ("Average Background", &mut self.feedback.avg_background, Some("Should be the field background without any foosmen. Will stop flickering on its own after a while")),
                    ("Non-Average White Mask", &mut self.feedback.non_avg_white, Some("Should only be the ball and the occasional single pixel column")),
                    ("Non-Average White Denoised Mask", &mut self.feedback.non_avg_white_denoised, Some("Should only be the ball")),
                    ("Ball & Rods Positions", &mut self.feedback.ball_and_rods_pos, Some("Ball should often be circled, and rods should be covered by lines of the matching color")),
                ]).zip(self.video_players.iter_mut()) {
                    vp.show(ui, ctx, name, feed, description, ui.available_width().max(self.config.cropping.width as f32));
                }
            });
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical_centered_justified(|ui| {
                ui.heading("Settings");
            });
            egui::ScrollArea::vertical().show(ui, |ui| {
                ui.add_space(3.0);
                ui.horizontal(|ui| {
                    if ui.button("Reset Settings").clicked() {
                        self.config = config::Config::default();
                    }
                    if ui.button("Load Settings").clicked() {
                        if let Ok(Some(filename)) =
                            native_dialog::FileDialog::new().show_open_single_file()
                        {
                            // try to load settings, or get a String explaining
                            // what went wrong
                            let error = if let Ok(mut file) = File::open(&filename) {
                                let mut contents = String::new();
                                match file.read_to_string(&mut contents) {
                                    Ok(_) => {
                                        if let Ok(loaded_config) = ron::de::from_str(&contents) {
                                            self.config = loaded_config;
                                            None
                                        } else {
                                            Some(format!(
                                                "File {:?} isn't formatted correctly",
                                                filename
                                            ))
                                        }
                                    }
                                    Err(err) => Some(format!(
                                        "Can't read file {:?} because {:?}",
                                        filename, err
                                    )),
                                }
                            } else {
                                Some(format!("Can't open file {:?}", filename))
                            };

                            // notify the user if any error occurred
                            if let Some(err) = error {
                                error_popup("Loading Error", &err);
                            }
                        }
                    }
                    if ui.button("Save Settings").clicked() {
                        if let Ok(Some(filename)) =
                            native_dialog::FileDialog::new().show_save_single_file()
                        {
                            let error = if let Ok(mut file) = OpenOptions::new()
                                .create(true)
                                .write(true)
                                .truncate(true)
                                .open(&filename)
                            {
                                if let Ok(serialized_config) = ron::ser::to_string_pretty(
                                    &self.config,
                                    ron::ser::PrettyConfig::new(),
                                ) {
                                    if let Ok(()) = file.write_all(serialized_config.as_bytes()) {
                                        None
                                    } else {
                                        Some(format!("Can't write to file {:?}", filename))
                                    }
                                } else {
                                    Some("Can't serialize current config".to_string())
                                }
                            } else {
                                Some(format!("Can't open file {:?}", filename))
                            };

                            if let Some(err) = error {
                                error_popup("Saving Error", &err)
                            }
                        }
                    }
                });
                ui.add_space(5.0);
                ui.horizontal(|ui| {
                    ui.label("Video Source");
                    egui::containers::ComboBox::from_id_source("video mode")
                        .selected_text(format!("{:?}", self.config.connection.connection_type))
                        .show_ui(ui, |ui| {
                            ui.selectable_value(
                                &mut self.config.connection.connection_type,
                                config::ConnectionType::RTSP,
                                "RTSP",
                            );
                            ui.selectable_value(
                                &mut self.config.connection.connection_type,
                                config::ConnectionType::Manual,
                                "Manual",
                            );
                        });
                });
                ui.add_space(3.0);
                ui.horizontal(|ui| {
                    match &mut self.config.connection.connection_type {
                        config::ConnectionType::RTSP => {
                            ui.add(
                                egui::TextEdit::singleline(&mut self.config.connection.rtsp_ip)
                                    .desired_width(95.0),
                            )
                            .on_hover_ui_at_pointer(|ui| {
                                ui.label("IPv4 address of the RTSP server");
                            });
                            ui.label(":");
                            ui.add(
                                egui::TextEdit::singleline(&mut self.config.connection.rtsp_port)
                                    .desired_width(40.0),
                            )
                            .on_hover_ui_at_pointer(|ui| {
                                ui.label("server port");
                            });
                        }
                        config::ConnectionType::Manual => {
                            ui.add(
                                egui::TextEdit::singleline(&mut self.config.connection.manual_url)
                                    .desired_width(200.0),
                            )
                            .on_hover_ui_at_pointer(|ui| {
                                ui.label("arbitrary url for video input");
                            });
                        }
                    }
                    if toggle(ui, self.video_processor_handle.is_some())
                        .on_hover_ui_at_pointer(|ui| {
                            ui.label(if self.video_processor_handle.is_none() {
                                "Connect"
                            } else {
                                "Disconnect"
                            });
                        })
                        .clicked()
                    {
                        self.video_processor_handle = match &self.video_processor_handle {
                            Some(vph) => {
                                vph.stop();
                                None
                            }
                            None => match VideoProcessor::start(&self.config.connection) {
                                Ok(vph) => vph
                                    .set_config(self.config.clone())
                                    .and(vph.set_feedback(self.feedback.clone()))
                                    .map_or(None, |_| Some(vph)),
                                Err(err) => {
                                    error_popup(
                                        "Connection Error",
                                        &format!(
                                            "Can't open {:?} because:\n{}",
                                            self.config.connection, err
                                        ),
                                    );
                                    None
                                }
                            },
                        }
                    }
                });
                ui.add_space(5.0);
                ui.collapsing("Camera Correction", |ui| {
                    let this_conf = &mut self.config.camera;
                    egui::Grid::new("unique_id_-1").show(ui, |ui| {
                        ui.label("Auto White Balance");
                        ui.checkbox(&mut this_conf.auto_white_balance, "");
                        ui.end_row();
                        LabeledDragValue::new("X Axis Angle", &mut this_conf.x_angle, -0.1..=0.1)
                            .suffix(" rad")
                            .show(ui);
                        LabeledDragValue::new("Y Axis Angle", &mut this_conf.y_angle, -0.1..=0.1)
                            .suffix(" rad")
                            .show(ui);
                    });
                });
                ui.collapsing("Table Orientation", |ui| {
                    let this_conf = &mut self.config.table_orientation_finder;
                    egui::Grid::new("unique_id_0").show(ui, |ui| {
                        LabeledDragValue::new(
                            "Update Interval",
                            &mut this_conf.table_orientation_update_interval,
                            0.1..=10.0,
                        )
                        .description(
                            "every how many seconds should the table orientation be updated",
                        )
                        .show(ui);
                        ui.label("Should Flip Table");
                        ui.checkbox(&mut this_conf.should_flip_table, "")
                            .on_hover_text("the red goalkeeper must be on the left!");
                        ui.end_row();
                        LabeledDragValue::new(
                            "Canny Threshold 1",
                            &mut this_conf.canny_threshold1,
                            0.0..=255.0,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "Canny Threshold 2",
                            &mut this_conf.canny_threshold2,
                            0.0..=255.0,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "Canny Aperture Size",
                            &mut this_conf.canny_aperture_size,
                            0..=7,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "Hough Angular Resolution",
                            &mut this_conf.hough_angle_res,
                            0.1..=5.0,
                        )
                        .suffix("°")
                        .show(ui);
                        LabeledDragValue::new(
                            "Hough Threshold",
                            &mut this_conf.hough_threshold,
                            0..=255,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "Hough Min Line Length",
                            &mut this_conf.hough_min_line_length,
                            0.0..=300.0,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "Hough Max Line Gap",
                            &mut this_conf.hough_max_line_gap,
                            0.0..=100.0,
                        )
                        .show(ui);
                    });
                });
                ui.collapsing("Table Position", |ui| {
                    let this_conf = &mut self.config.table_position_finder;
                    egui::Grid::new("unique_id_0.5").show(ui, |ui| {
                        ui.label("Enable Fixed Table Width");
                        ui.checkbox(&mut this_conf.enable_fixed_width, "");
                        ui.end_row();
                        LabeledDragValue::new("Table Width", &mut this_conf.table_width, 0.0..=1.0)
                            .show(ui);
                        LabeledDragValue::new(
                            "Aspect Ratio",
                            &mut this_conf.aspect_ratio,
                            1.4..=1.75,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "X Axis Padding",
                            &mut this_conf.x_padding,
                            -0.1..=0.1,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "Y Axis Padding",
                            &mut this_conf.y_padding,
                            -0.1..=0.1,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "X Axis Offset",
                            &mut this_conf.x_offset,
                            -0.02..=0.02,
                        )
                        .show(ui);
                        LabeledDragValue::new(
                            "Y Axis Offset",
                            &mut this_conf.y_offset,
                            -0.02..=0.02,
                        )
                        .show(ui);
                    });
                });
                ui.collapsing("Cropping", |ui| {
                    let this_conf = &mut self.config.cropping;
                    egui::Grid::new("unique_id_1").show(ui, |ui| {
                        LabeledDragValue::new("Width", &mut this_conf.width, 100..=1000).show(ui);
                        LabeledDragValue::new("Height", &mut this_conf.height, 100..=1000).show(ui);
                    });
                });
                ui.collapsing("Colors", |ui| {
                    let this_conf = &mut self.config.colors;
                    egui::Grid::new("unique_id_2").show(ui, |ui| {
                        for (color_name, color_value) in [
                            ("Green Low", &mut this_conf.green_low),
                            ("Green High", &mut this_conf.green_high),
                            ("White Low", &mut this_conf.white_low),
                            ("White High", &mut this_conf.white_high),
                            ("Red Low", &mut this_conf.red_low),
                            ("Red High", &mut this_conf.red_high),
                            ("Blue Low", &mut this_conf.blue_low),
                            ("Blue High", &mut this_conf.blue_high),
                        ] {
                            ui.label(color_name);
                            color_chooser(ui, color_value);
                            ui.end_row();
                        }
                    });
                });
                ui.collapsing("Ball", |ui| {
                    let this_conf = &mut self.config.ball_finder;
                    egui::Grid::new("unique_id_3").show(ui, |ui| {
                        LabeledDragValue::new(
                            "Mask Blur Size",
                            &mut this_conf.mask_blur_size,
                            1..=21,
                        )
                        .show(ui);
                        LabeledDragValue::new("Alpha", &mut this_conf.alpha, 0.001..=0.02).show(ui);
                        LabeledDragValue::new(
                            "Noise Threshold",
                            &mut this_conf.noise_threshold,
                            0.0..=255.0,
                        )
                        .show(ui);
                        LabeledDragValue::new("Ball Size", &mut this_conf.ball_size, 1..=25)
                            .show(ui);
                        LabeledDragValue::new(
                            "Ball Threshold",
                            &mut this_conf.ball_threshold,
                            0.0..=255.0,
                        )
                        .show(ui);
                    });
                });
                ui.collapsing("Rods", |ui| {
                    let this_conf = &mut self.config.rods_finder;
                    egui::Grid::new("unique_id_4").show(ui, |ui| {
                        LabeledDragValue::new(
                            "Consec Rods Dist",
                            &mut this_conf.rods_dist,
                            0.1..=0.15,
                        )
                        .speed(0.0005)
                        .suffix(" · table length")
                        .show(ui);
                        for (i, range) in [0.6..=0.7, 0.35..=0.45, 0.7..=0.8, 0.6..=0.7]
                            .iter()
                            .enumerate()
                        {
                            LabeledDragValue::new(
                                &format!("Rod Width {}", i),
                                &mut this_conf.rods_width[i],
                                range.clone(),
                            )
                            .speed(0.001)
                            .suffix(" · table width")
                            .show(ui);
                        }
                        LabeledDragValue::new(
                            "Foosmen Width",
                            &mut this_conf.foosmen_width,
                            0.001..=0.01,
                        )
                        .speed(0.0001)
                        .suffix(" · table width")
                        .show(ui);
                    });
                });
            });
        });

        if self.config != self.old_config {
            if let Some(vph) = &self.video_processor_handle {
                let _ = vph.set_config(self.config.clone());
            }
            self.old_config = self.config.clone();
        }

        if self.feedback != self.old_feedback {
            if let Some(vph) = &self.video_processor_handle {
                let _ = vph.set_feedback(self.feedback.clone());
            }
            self.old_feedback = self.feedback.clone();
        }

        if let Some(vph) = &self.video_processor_handle {
            if vph.has_disconnected() {
                self.video_processor_handle = None;
            }
        }

        ctx.request_repaint();
    }

    fn on_exit(&mut self, _gl: Option<&eframe::glow::Context>) {
        if let Some(vph) = &self.video_processor_handle {
            vph.stop();
        }
    }
}
