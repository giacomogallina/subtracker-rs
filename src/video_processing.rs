//! This is where the real work happens

// stdlib imports
use std::error::Error;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::time::{SystemTime, UNIX_EPOCH};

// external lib imports
use opencv::core::{Mat, Point};

// module declarations
/// This module is for finding the ball
pub mod ball;
/// Configuration for the [`VideoProcessor`]
pub mod config;
/// Tools to deal with the frame processing results
pub mod data_output;
/// A module for easy parallelization of workloads composed
/// of a sequence of steps
pub mod pipeline;
/// Finding the rods' positions
pub mod rods;
/// This module has the structs necessary to find the table's orientation and position
pub mod table;
/// Common utility functions
pub mod utils;
/// Module for reading frames from a video feed
pub mod video_input;

// module imports
use ball::BallFinder;
use config::Config;
use data_output::OutputData;
use rods::RedBlueMaskGenerator;
use rods::RodsFinder;
use table::TableOrientationFinder;
use table::TablePositionFinder;
use video_input::VideoStream;

use self::ball::AnalysisDrawer;
use self::pipeline::{Pipeline, PipelineBuilder, PipelineInfo};
use self::video_input::{correct_camera_angle, WhiteBalancer};

/// Type of commands that the [`GUI`][crate::gui] can give to the [`VideoProcessor`]
enum VideoProcessorMessage {
    NewConfig(Config),
    NewFeedback(Feedback),
    Stop,
    /// Used to check if the [`VideoProcessor`]'s video input is still connnected
    HeartBeat,
}

/// Handles the interactions between the [`GUI`][crate::gui] and the [`VideoProcessor`]
pub struct VideoProcessorHandle {
    vp_message_sender: Sender<VideoProcessorMessage>,
}

impl VideoProcessorHandle {
    /// Sends a new config to the [`VideoProcessor`]
    pub fn set_config(&self, conf: Config) -> Result<(), ()> {
        self.vp_message_sender
            .send(VideoProcessorMessage::NewConfig(conf))
            .map_err(|_| ())
    }

    /// Changes what feedbacks the [`VideoProcessor`] needs to send back
    pub fn set_feedback(&self, feed: Feedback) -> Result<(), ()> {
        self.vp_message_sender
            .send(VideoProcessorMessage::NewFeedback(feed))
            .map_err(|_| ())
    }

    /// Tells the [`VideoProcessor`] to stop working, and
    /// after this the [`VideoProcessor`] needs to be sostituted
    pub fn stop(&self) {
        // we don't care if the vp is already dead, since we want to stop it
        let _ = self.vp_message_sender.send(VideoProcessorMessage::Stop);
    }

    /// Checks if the [`VideoProcessor`] has disconnected from its video input
    pub fn has_disconnected(&self) -> bool {
        self.vp_message_sender
            .send(VideoProcessorMessage::HeartBeat)
            .is_err()
    }
}

#[derive(Clone, Default)]
/// A bunch of channel [`Sender`]s used by the [`VideoProcessor`]'s [`Pipeline`]
/// to send some intermediate results back to the [`GUI`][crate::gui]
pub struct Feedback {
    pub original: Option<Sender<Mat>>,
    pub rotated: Option<Sender<Mat>>,
    pub canny_and_hough: Option<Sender<Mat>>,
    pub rotated_green: Option<Sender<Mat>>,
    pub cropped: Option<Sender<Mat>>,
    pub colors: Option<Sender<Mat>>,
    pub avg_background: Option<Sender<Mat>>,
    pub non_avg_white: Option<Sender<Mat>>,
    pub non_avg_white_denoised: Option<Sender<Mat>>,
    pub ball_and_rods_pos: Option<Sender<Mat>>,
    pub pipeline_info: Option<Sender<PipelineInfo>>,
}

impl PartialEq for Feedback {
    fn eq(&self, other: &Self) -> bool {
        self.original.is_some() == other.original.is_some()
            && self.rotated.is_some() == other.rotated.is_some()
            && self.canny_and_hough.is_some() == other.canny_and_hough.is_some()
            && self.rotated_green.is_some() == other.rotated_green.is_some()
            && self.cropped.is_some() == other.cropped.is_some()
            && self.colors.is_some() == other.colors.is_some()
            && self.avg_background.is_some() == other.avg_background.is_some()
            && self.non_avg_white.is_some() == other.non_avg_white.is_some()
            && self.non_avg_white_denoised.is_some() == other.non_avg_white_denoised.is_some()
            && self.ball_and_rods_pos.is_some() == other.ball_and_rods_pos.is_some()
            && self.pipeline_info.is_some() == other.pipeline_info.is_some()
    }
}

/// Struct that reads frames from a [`VideoStream`] and processes them in a
/// [`Pipeline`]
pub struct VideoProcessor {
    vp_message_receiver: Receiver<VideoProcessorMessage>,
    config: Config,
    feedback: Feedback,
    pipeline: Pipeline<Mat>,
}

impl VideoProcessor {
    /// Builds a new `VideoProcessor`, starts it in a separate thread and
    /// returns a `VideoProcessorHandle` that can be used to interact with it
    pub fn start(
        connection: &config::ConnectionConfig,
    ) -> Result<VideoProcessorHandle, Box<dyn Error>> {
        let (vp_message_sender, vp_message_receiver) = channel();
        let feedback = Feedback::default();
        let config = Config::default();

        let pipeline = PipelineBuilder::<Mat, Mat>::new()
            .add_step("White Balancing", WhiteBalancer::new())
            .add_step(
                "Camera Correction",
                |config: &Config, feed: &Feedback, _timestamp: f64, src: Mat| {
                    let corrected_src = correct_camera_angle(config.camera, src)?;
                    if let Some(feed) = &feed.original {
                        feed.send(corrected_src.clone()).unwrap();
                    }
                    Ok(corrected_src)
                },
            )
            .add_step("Table Orientation", TableOrientationFinder::new())
            .add_step("Table Position", TablePositionFinder::new())
            .add_step("Masking", RedBlueMaskGenerator::new())
            .add_step("Rods", RodsFinder::new())
            .add_step("Ball", BallFinder::new())
            .add_step("Drawing Analysis", AnalysisDrawer::default())
            .add_step(
                "Printing",
                |config: &Config,
                 _feed: &Feedback,
                 timestamp: f64,
                 ((rods_position, rods_rotation), ball_position): (
                    ([f32; 8], [f32; 8]),
                    Option<Point>,
                )| {
                    let (w, h) = (config.cropping.width as f32, config.cropping.height as f32);
                    println!(
                        "{}",
                        OutputData {
                            timestamp,
                            rods_position,
                            rods_rotation,
                            ball_position: ball_position.map(|p| (p.x as f32 / w, p.y as f32 / h))
                        }
                        .serialize()
                    );
                    Ok(())
                },
            )
            .start();

        let mut vp = VideoProcessor {
            vp_message_receiver,
            config,
            feedback,
            pipeline,
        };

        let video_stream = VideoStream::new(&connection)?;
        thread::Builder::new()
            .name("video processor thread".to_string())
            .spawn(move || {
                vp.run(video_stream);
            })
            .expect("couldn't spawn video processor thread");

        Ok(VideoProcessorHandle { vp_message_sender })
    }

    /// Iterates over the frames read from `jpeg_stream` and gives them to the `Pipeline`,
    /// while listening for commands from the `VideoProcessorHandle`
    fn run(&mut self, video_stream: VideoStream) {
        'main: for (_timestamp, frame) in video_stream {
            // before processing this frame, we check if the VideoProcessorHandle
            // sent us any command
            while let Ok(msg) = self.vp_message_receiver.try_recv() {
                match msg {
                    VideoProcessorMessage::NewConfig(conf) => {
                        self.config = conf;
                    }
                    VideoProcessorMessage::NewFeedback(feed) => {
                        self.feedback = feed;
                    }
                    VideoProcessorMessage::Stop => {
                        break 'main;
                    }
                    VideoProcessorMessage::HeartBeat => {
                        // just receiving the message means that the VideoProcessor
                        // is still alive
                    }
                }
            }

            let unix_timestamp = SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .map_or(0.0, |d| d.as_secs_f64());

            self.pipeline.process(
                self.config.clone(),
                self.feedback.clone(),
                unix_timestamp,
                frame,
            );
        }
    }
}
