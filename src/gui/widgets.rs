use std::{
    collections::VecDeque,
    sync::mpsc::{channel, Receiver, Sender},
};

use eframe::egui::{
    self,
    ecolor::{hsv_from_rgb, Hsva},
    emath::Numeric,
    load::SizedTexture,
    Color32, DragValue, Image, ImageSource, TextureHandle, Ui,
};
use egui_plot::{Bar, BarChart, Legend, Plot};
use opencv::{
    core::{Mat, MatTraitConst, Vec4b},
    imgproc::{cvt_color, COLOR_BGR2RGBA, COLOR_GRAY2RGBA},
    // prelude::MatTraitConstManual,
};

use crate::video_processing::{config, pipeline::PipelineInfo};

/// A [`DragValue`] with a [`Label`][egui::Label] on its left
pub struct LabeledDragValue<'a, Num>
where
    Num: Numeric + core::ops::Sub<Output = Num> + Into<f64>,
{
    label: &'a str,
    value: &'a mut Num,
    range: core::ops::RangeInclusive<Num>,
    speed: Option<f64>,
    suffix: &'a str,
    description: Option<&'a str>,
}

impl<'a, Num> LabeledDragValue<'a, Num>
where
    Num: Numeric + core::ops::Sub<Output = Num> + Into<f64>,
{
    pub fn new(label: &'a str, value: &'a mut Num, range: core::ops::RangeInclusive<Num>) -> Self {
        LabeledDragValue {
            label,
            value,
            range,
            speed: None,
            suffix: "",
            description: None,
        }
    }

    pub fn speed(mut self, speed: f64) -> Self {
        self.speed = Some(speed);
        self
    }

    pub fn suffix(mut self, suffix: &'a str) -> Self {
        self.suffix = suffix;
        self
    }

    pub fn description(mut self, description: &'a str) -> Self {
        self.description = Some(description);
        self
    }

    pub fn show(self, ui: &mut Ui) {
        ui.label(self.label);
        let range_width: f64 = (*self.range.end() - *self.range.start()).into();
        let res = ui.add(
            DragValue::new(self.value)
                .clamp_range(self.range)
                .suffix(self.suffix)
                .max_decimals((-range_width.log10() + 1.5).max(0.0).round() as usize)
                .speed(self.speed.unwrap_or(range_width / 100.0)),
        );
        if let Some(description) = self.description {
            res.on_hover_text(description);
        }
        ui.end_row();
    }
}

/// selects a color
pub fn color_chooser(ui: &mut Ui, color: &mut config::HSVColor) {
    ui.horizontal(|ui| {
        ui.label("H:");
        ui.add(DragValue::new(&mut color.h).clamp_range(0.0..=180.0));
        ui.label("S:");
        ui.add(DragValue::new(&mut color.s).clamp_range(0.0..=255.0));
        ui.label("V:");
        ui.add(DragValue::new(&mut color.v).clamp_range(0.0..=255.0));
        ui.colored_label(
            Hsva::new(
                color.h as f32 / 180.0,
                color.s as f32 / 255.0,
                color.v as f32 / 255.0,
                1.0,
            ),
            "⬛",
        );
    });
}

/// Selects a color range
#[allow(dead_code)]
pub fn color_range(ui: &mut Ui, low: &mut config::HSVColor, high: &mut config::HSVColor) {
    ui.horizontal(|ui| {
        ui.label("(");
        ui.add(DragValue::new(&mut low.h).clamp_range(0.0..=180.0));
        ui.label(",");
        ui.add(DragValue::new(&mut low.s).clamp_range(0.0..=255.0));
        ui.label(",");
        ui.add(DragValue::new(&mut low.v).clamp_range(0.0..=255.0));
        ui.label(")  = ");
        ui.colored_label(
            Hsva::new(
                low.h as f32 / 180.0,
                low.s as f32 / 255.0,
                low.v as f32 / 255.0,
                1.0,
            ),
            "⬛",
        );
        ui.label(" ≤  ( H, S, V )  ≤ ");
        ui.colored_label(
            Hsva::new(
                high.h as f32 / 180.0,
                high.s as f32 / 255.0,
                high.v as f32 / 255.0,
                1.0,
            ),
            "⬛",
        );
        ui.label(" =  (");
        ui.add(DragValue::new(&mut high.h).clamp_range(0.0..=180.0));
        ui.label(",");
        ui.add(DragValue::new(&mut high.s).clamp_range(0.0..=255.0));
        ui.label(",");
        ui.add(DragValue::new(&mut high.v).clamp_range(0.0..=255.0));
        ui.label(")");
    });
}

pub struct VideoPlayer {
    frame_sender: Sender<Mat>,
    frame_receiver: Receiver<Mat>,
    current_frame_rgba: Mat,
    current_texture: Option<TextureHandle>,
    zooming: bool,
}

impl VideoPlayer {
    pub fn new() -> Self {
        let (frame_sender, frame_receiver) = channel();
        VideoPlayer {
            frame_sender,
            frame_receiver,
            current_frame_rgba: Mat::default(),
            current_texture: None,
            zooming: false,
        }
    }

    pub fn show(
        &mut self,
        ui: &mut Ui,
        ctx: &egui::Context,
        name: &str,
        feed: &mut Option<Sender<Mat>>,
        description: Option<&str>,
        max_width: f32,
    ) {
        // check if we received a new frame to display
        let mut new_frame = None;
        while let Ok(frame) = self.frame_receiver.try_recv() {
            new_frame = Some(frame);
        }
        // if we did, we need to convert it from opencv Mat to egui texture
        // but not if we are zoomed, since the video must be paused while zooming
        if let (Some(new_frame), false) = (new_frame, self.zooming) {
            // first free the old texture
            if let Some(texture_handle) = &self.current_texture {
                (*(*ctx.tex_manager()).write()).free(texture_handle.id());
            }
            let (w, h) = (new_frame.cols(), new_frame.rows());
            // then we convert it to the RGBA colospace
            match new_frame.channels() {
                1 => {
                    cvt_color(&new_frame, &mut self.current_frame_rgba, COLOR_GRAY2RGBA, 0)
                        .unwrap();
                }
                3 => {
                    cvt_color(&new_frame, &mut self.current_frame_rgba, COLOR_BGR2RGBA, 0).unwrap();
                }
                n => panic!("last_new_frame had {} channels, instead of 1 or 3", n),
            };
            // the following transmutation will read random memory if the matrix isn't continuous!
            assert!(self.current_frame_rgba.is_continuous());
            // then we transmute the mat's underlying data to a slice
            let pixels = unsafe {
                // plz dont segfault
                std::slice::from_raw_parts(
                    std::mem::transmute(self.current_frame_rgba.data()),
                    (w * h * 4) as usize,
                )
            };
            // we finally have our new texture!
            self.current_texture = Some(ctx.load_texture(
                "",
                egui::ColorImage::from_rgba_unmultiplied([w as usize, h as usize], pixels),
                Default::default(),
            ));
        }
        // if the videoplayer is open, we need to draw the current frame
        if feed.is_some() {
            ui.vertical(|ui| {
                if ui
                    .add(egui::Button::new(&format!("➖  {}", name)).frame(false))
                    .clicked()
                {
                    *feed = None;
                }
                if let Some(tex) = &self.current_texture {
                    let (w, h) = (
                        self.current_frame_rgba.cols(),
                        self.current_frame_rgba.rows(),
                    );
                    let scale = (max_width / w as f32).min(1.0);
                    let (w, h) = (w as f32 * scale, h as f32 * scale);
                    let mut uv = [egui::Pos2::new(0.0, 0.0), egui::Pos2::new(1.0, 1.0)];
                    let mut hovered_color = None;
                    let pos = ui.input(|i| i.pointer.hover_pos());
                    if let Some(pos) = pos {
                        let rel_pos = (pos
                            - ui.min_rect().left_bottom()
                            - egui::Vec2::new(0.0, ui.spacing().item_spacing.y))
                        .to_pos2();
                        let (x, y) = (rel_pos.x / w, rel_pos.y / h);
                        if 0.0 <= x && x <= 1.0 && 0.0 <= y && y <= 1.0 {
                            if self.zooming {
                                let pad = 0.2;
                                let (x, y) = (
                                    ((x - pad) / (1.0 - 2.0 * pad)).clamp(0.0, 1.0),
                                    ((y - pad) / (1.0 - 2.0 * pad)).clamp(0.0, 1.0),
                                );
                                uv = [
                                    egui::Pos2::new(0.5 * x, 0.5 * y),
                                    egui::Pos2::new(0.5 * x + 0.5, 0.5 * y + 0.5),
                                ];
                                hovered_color = self
                                    .current_frame_rgba
                                    .at_2d::<Vec4b>(
                                        (0.5 * (rel_pos.y + y * h) / scale) as i32,
                                        (0.5 * (rel_pos.x + x * w) / scale) as i32,
                                    )
                                    .ok();
                            } else {
                                hovered_color = self
                                    .current_frame_rgba
                                    .at_2d::<Vec4b>(
                                        (rel_pos.y / scale) as i32,
                                        (rel_pos.x / scale) as i32,
                                    )
                                    .ok();
                            }
                        } else {
                            self.zooming = false;
                        }
                    }
                    let img = ui
                        .add(
                            Image::new(ImageSource::Texture(SizedTexture::new(tex.id(), (w, h))))
                                .uv(uv)
                                .sense(egui::Sense::click()),
                        )
                        .on_hover_cursor(if self.zooming {
                            egui::CursorIcon::ZoomOut
                        } else {
                            egui::CursorIcon::ZoomIn
                        });
                    if img.clicked() {
                        self.zooming = !self.zooming;
                    };
                    img.on_hover_ui_at_pointer(|ui| {
                        if let Some(description) = description {
                            ui.label(description);
                        }
                        if let Some(color) = hovered_color {
                            ui.horizontal(|ui| {
                                let (h, s, v) = hsv_from_rgb([
                                    color[0] as f32 / 255.0,
                                    color[1] as f32 / 255.0,
                                    color[2] as f32 / 255.0,
                                ]);
                                ui.label(&format!(
                                    "HSV: ({}, {}, {})",
                                    (h * 180.0) as i32,
                                    (s * 255.0) as i32,
                                    (v * 255.0) as i32
                                ));
                                ui.colored_label(
                                    Color32::from_rgb(color[0], color[1], color[2]),
                                    "⬛",
                                );
                            });
                        }
                    });
                }
            });
        } else if ui
            .add(egui::Button::new(&format!("➕  {}", name)).frame(false))
            .clicked()
        {
            *feed = Some(self.frame_sender.clone());
        }
    }
}

// shamelessly stolen from https://github.com/emilk/egui/blob/master/egui_demo_lib/src/apps/demo/toggle_switch.rs
pub fn toggle(ui: &mut egui::Ui, on: bool) -> egui::Response {
    let desired_size = ui.spacing().interact_size.y * egui::vec2(2.0, 1.0);
    let (rect, response) = ui.allocate_exact_size(desired_size, egui::Sense::click());
    response.widget_info(|| egui::WidgetInfo::selected(egui::WidgetType::Checkbox, on, ""));

    let how_on = ui.ctx().animate_bool(response.id, on);
    let visuals = ui.style().interact_selectable(&response, on);
    let rect = rect.expand(visuals.expansion);
    let radius = 0.5 * rect.height();
    ui.painter()
        .rect(rect, radius, visuals.bg_fill, visuals.bg_stroke);
    let circle_x = egui::lerp((rect.left() + radius)..=(rect.right() - radius), how_on); //if on { 1.0 } else { 0.0 });
    let center = egui::pos2(circle_x, rect.center().y);
    ui.painter()
        .circle(center, 0.75 * radius, visuals.bg_fill, visuals.fg_stroke);

    response
}

const SAMPLES: usize = 256;
pub struct PipelineInfoPlot {
    pipeline_infos: VecDeque<PipelineInfo>,
    info_sender: Sender<PipelineInfo>,
    info_receiver: Receiver<PipelineInfo>,
}

impl PipelineInfoPlot {
    pub fn new() -> Self {
        let (info_sender, info_receiver) = channel();
        Self {
            pipeline_infos: VecDeque::new(),
            info_sender,
            info_receiver,
        }
    }

    pub fn show(&mut self, ui: &mut Ui, feed: &mut Option<Sender<PipelineInfo>>) {
        if feed.is_none() {
            *feed = Some(self.info_sender.clone());
        }
        while let Ok(info) = self.info_receiver.try_recv() {
            self.pipeline_infos.push_front(info);
        }
        while self.pipeline_infos.len() > SAMPLES {
            self.pipeline_infos.pop_back();
        }
        // if !self.pipeline_infos.is_empty() {
        // }
        // for j in self.
        let mut bars = vec![];
        let mut names = vec![];
        for (i, info) in self.pipeline_infos.iter().enumerate() {
            for (j, (name, time)) in info.timings.iter().enumerate() {
                if bars.len() <= j {
                    bars.push(vec![]);
                    names.push(name.to_string());
                }
                bars[j].push(Bar::new(-(1.0 + i as f64), time.as_secs_f64()));
            }
        }
        let mut bar_charts: Vec<BarChart> = vec![];
        for (i, (bar, name)) in bars.iter().zip(names).enumerate() {
            bar_charts.push(
                BarChart::new(bar.clone()).width(1.0).name(name).stack_on(
                    (0..i)
                        .map(|j| &bar_charts[j])
                        .collect::<Vec<&BarChart>>()
                        .as_ref(),
                ),
            );
        }
        Plot::new("test")
            .allow_zoom(false)
            .allow_scroll(false)
            .allow_drag(false)
            .allow_boxed_zoom(false)
            .legend(Legend::default().position(egui_plot::Corner::LeftTop))
            .show(ui, |plot_ui| {
                for bar_chart in bar_charts {
                    plot_ui.bar_chart(bar_chart);
                }
            });
    }
}
