#![allow(rustdoc::private_intra_doc_links)]
#![doc(html_logo_url = "../../../res/logo2.png")]
#![doc(html_favicon_url = "../../../res/logo2.png")]
///////////////////////////////////////////////////////
//   it's a good idea to run                         //
//   cargo doc --open                                //
//   to easily browse this program's documentation   //
///////////////////////////////////////////////////////

//! # Introduction
//! `subtracker-rs` is a reimplementation of `subtracker` using Rust. As such,
//! its job is to receive a video of a foosball match and analyze it, finding for every
//! frame the position of the ball and rods
//!
//! Reimplementation doesn't mean port: `subtracker-rs` tries to do the same work as
//! `subtracker`, but the inner structure and functioning are different: for example,
//! the table isn't tracked using optical flow, and no hand-made images/masks are
//! required. Instead `subtracker-rs` only wants a config file (or not, if the default
//! settings are good enough).
//!
//!
//! # Assumtions / Limitations
//! In order for `subtracker-rs` to work correctly, these assumpions must hold:
//! - the field background must be roughly of the same color, which must be
//! different from the color of the table's structure
//! - the foosmen must have a color different from the field color
//! - the field should have as little area as possible of the same color as the ball
//! (so no white field and ball, but a few white lines are ok)
//!
//!
//! # Architecture
//!
//! ```text
//!                                   _())__()_ _Oo_
//!                                ,_(         )    )_
//!                               ((  Video Source  )_o_)))
//! ┌────────────────────┐      o(_____~_______~___~____())
//! │         GUI        │                 │
//! ├────────────────────┤                 │
//! │ ┌──────────────────┴───┐             │      ┌──────────────────┐
//! │ │ VideoProcessorHandle │             │      │  VideoProcessor  │
//! │ ├──────────────────────┤             │      ├──────────────────┤
//! │ │ ┌────────────────────┴─┐           │    ┌─┴─────────────┐    │              
//! │ │ │   vp_message_sender  │──────┐    └───►│  VideoStream  │    │                 
//! │ │ └────────────────────┬─┘      │         └─┬─────────────┘    │               
//! │ └──────────────────┬───┘        │      ┌────┴────────────────┐ │       
//! │ ┌──────────────────┴───┐        └─────►│ vp_message_receiver │ │       
//! │ │  Feedback receivers  │◄────┐         └────┬────────────────┘ │             
//! │ └──────────────────┬───┘     │          ┌───┴────────────────┐ │
//! └────────────────────┘         └──────────│  Feedback senders  │ │
//!                                           └───┬────────────────┘ │
//! ┌────────────────────┐                      ┌─┴────────────┐     │
//! │   PipelineStep 1   │◄─────────────────────│   Pipeline   │     │
//! └─────────┬──────────┘                      └─┬────────────┘     │
//! ┌─────────┴──────────┐                        └──────────────────┘       
//! │   PipelineStep 2   │      
//! └─────────┬──────────┘      
//!          ...               
//!           └────────────────────────►  stdout
//! ```
//!
//! The main program components are the [`GUI`][gui] and the [`VideoProcessor`][video_processing::VideoProcessor]
//!
//! Every time the [`GUI`][gui] successfully connects to a video source,
//! it creates a [`VideoProcessor`][video_processing::VideoProcessor] that starts working,
//! reading and analyzing the frames from the video source.
//!
//! The other job of the [`VideoProcessor`][video_processing::VideoProcessor] is to produce the
//! frames of the intermediate results feedback videos and to send them to the [`GUI`][gui] through the
//! [`Feedback senders`][video_processing::VideoProcessor::feedback] (these are the videos on the left
//! side of the gui that are used for feedback when the user needs to change the program settings).
//!
//! The [`GUI`][gui] keeps a [`VideoProcessorHandle`][video_processing::VideoProcessorHandle] with which
//! it communicates with the [`VideoProcessor`][video_processing::VideoProcessor], to change its
//! [`settings`][video_processing::VideoProcessor::config], to change its
//! [`Feedback senders`][video_processing::VideoProcessor::feedback], and to check if the
//! [`VideoProcessor`][video_processing::VideoProcessor] is still working.
//!
//! When a new frame arrives, the [`VideoProcessor`][video_processing::VideoProcessor] sends it into its
//! [`Pipeline`][video_processing::pipeline::Pipeline], which is composed of a sequence of
//! [`PipelineStep`s][video_processing::pipeline::PipelineStep], each running in its own thread.
//! These steps perform various tasks on the incoming data, possibly produce some feedbacks for the `GUI`,
//! and in the end print the completed analysis to `stdout`

mod gui;
mod video_processing;

use gui::Gui;

fn main() {
    Gui::new().run();
}
