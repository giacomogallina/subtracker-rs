use sqlx::PgConnection;

use super::pipeline::PipelineStep;

const TABLE_X: f32 = 1.15;
const TABLE_Y: f32 = 0.70;

#[derive(Copy, Clone)]
/// the data that the database wants
pub struct OutputData {
    pub timestamp: f64,
    pub rods_position: [f32; 8],
    pub rods_rotation: [f32; 8],
    pub ball_position: Option<(f32, f32)>,
}

impl OutputData {
    /// Formats the data as a CSV row, in this format:
    ///
    /// `timestamp,ball_x,ball_y,red_goalkeeper_pos,red_goalkeeper_rot,red_defender_pos,red_defender_rot,...,blue_forward_pos,blue_forward_rot`
    ///
    /// All measures are expressed in meters, the ball position has its origin in the center of the field
    /// and its positive axis go right and down, all `pos` are measured from the rod's central position
    /// going downward and all `rot` are `0` because we don't calculate them yet
    pub fn serialize(&self) -> String {
        format!(
            "{},{}{}",
            self.timestamp,
            match self.ball_position {
                Some((x, y)) => format!("{},{}", (x - 0.5) * TABLE_X, -(y - 0.5) * TABLE_Y),
                None => ",".to_string(),
            },
            [0, 1, 3, 5, 7, 6, 4, 2]
                .iter()
                .map(|&i| format!(
                    ",{},{}",
                    -(self.rods_position[i]) * TABLE_Y,
                    self.rods_rotation[i]
                ))
                .fold(String::new(), |acc, s| acc + &s)
        )
    }
}

pub struct DataSaver {
    conn: Option<PgConnection>,
}

impl DataSaver {
    pub fn new() -> Self {
        Self { conn: None }
    }
}

impl PipelineStep<OutputData, ()> for DataSaver {
    fn process(
        &mut self,
        conf: &super::config::Config,
        feed: &super::Feedback,
        timestamp: f64,
        data: OutputData,
    ) -> Result<(), Box<dyn std::error::Error>> {
        todo!()
    }
}
