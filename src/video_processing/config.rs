use serde::{Deserialize, Serialize};

/// Main config for the [`VideoProcessor`][super::VideoProcessor]
#[derive(Clone, PartialEq, Serialize, Deserialize)]
pub struct Config {
    pub connection: ConnectionConfig,
    pub camera: CameraConfig,
    pub table_orientation_finder: TableOrientationFinderConfig,
    pub table_position_finder: TablePositionFinderConfig,
    pub cropping: CroppingConfig,
    pub ball_finder: BallFinderConfig,
    pub colors: ColorsConfig,
    pub rods_finder: RodsFinderConfig,
}

impl Config {
    pub fn default() -> Self {
        Config {
            connection: ConnectionConfig::default(),
            camera: CameraConfig::default(),
            table_orientation_finder: TableOrientationFinderConfig::default(),
            table_position_finder: TablePositionFinderConfig::default(),
            colors: ColorsConfig::default(),
            cropping: CroppingConfig::default(),
            ball_finder: BallFinderConfig::default(),
            rods_finder: RodsFinderConfig::default(),
        }
    }
}

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub enum ConnectionType {
    RTSP,
    Manual,
}

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct ConnectionConfig {
    pub connection_type: ConnectionType,
    pub rtsp_ip: String,
    pub rtsp_port: String,
    pub manual_url: String,
}

impl ConnectionConfig {
    pub fn default() -> Self {
        Self {
            connection_type: ConnectionType::RTSP,
            rtsp_ip: "0.0.0.0".to_string(),
            rtsp_port: "8554".to_string(),
            manual_url: "rtsp://0.0.0.0:8554/subtracker".to_string(),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct CameraConfig {
    pub auto_white_balance: bool,
    pub x_angle: f32,
    pub y_angle: f32,
}

impl Default for CameraConfig {
    fn default() -> Self {
        Self {
            auto_white_balance: true,
            x_angle: 0.0,
            y_angle: 0.0,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct TableOrientationFinderConfig {
    pub table_orientation_update_interval: f64,
    pub should_flip_table: bool,
    pub canny_threshold1: f64,
    pub canny_threshold2: f64,
    pub canny_aperture_size: i32,
    pub hough_angle_res: f64,
    pub hough_threshold: i32,
    pub hough_min_line_length: f64,
    pub hough_max_line_gap: f64,
}

impl TableOrientationFinderConfig {
    fn default() -> Self {
        TableOrientationFinderConfig {
            table_orientation_update_interval: 1.0,
            should_flip_table: false,
            canny_threshold1: 80.0,
            canny_threshold2: 200.0,
            canny_aperture_size: 3,
            hough_angle_res: 1.0,
            hough_threshold: 20,
            hough_min_line_length: 100.0,
            hough_max_line_gap: 20.0,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct TablePositionFinderConfig {
    pub enable_fixed_width: bool,
    pub table_width: f32,
    pub aspect_ratio: f32,
    pub x_padding: f32,
    pub y_padding: f32,
    pub x_offset: f32,
    pub y_offset: f32,
}

impl TablePositionFinderConfig {
    fn default() -> Self {
        TablePositionFinderConfig {
            enable_fixed_width: false,
            table_width: 0.5,
            aspect_ratio: 1.49,
            x_padding: 0.05,
            y_padding: 0.0,
            x_offset: 0.0,
            y_offset: 0.0,
        }
    }
}

/// The various colors used by many steps of the frame processing
#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct ColorsConfig {
    pub green_low: HSVColor,
    pub green_high: HSVColor,
    pub red_low: HSVColor,
    pub red_high: HSVColor,
    pub blue_low: HSVColor,
    pub blue_high: HSVColor,
    pub white_low: HSVColor,
    pub white_high: HSVColor,
}

impl ColorsConfig {
    fn default() -> Self {
        ColorsConfig {
            green_low: HSVColor {
                h: 55.0,
                s: 80.0,
                v: 50.0,
            },
            green_high: HSVColor {
                h: 90.0,
                s: 255.0,
                v: 200.0,
            },

            red_low: HSVColor {
                h: 175.0,
                s: 100.0,
                v: 50.0,
            },
            red_high: HSVColor {
                h: 15.0,
                s: 255.0,
                v: 255.0,
            },

            blue_low: HSVColor {
                h: 95.0,
                s: 150.0,
                v: 50.0,
            },
            blue_high: HSVColor {
                h: 110.0,
                s: 255.0,
                v: 255.0,
            },

            white_low: HSVColor {
                h: 20.0,
                s: 0.0,
                v: 170.0,
            },
            white_high: HSVColor {
                h: 150.0,
                s: 80.0,
                v: 255.0,
            },
        }
    }
}

/// HSV color where 0 <= h <= 180 and 0 <= s, v <= 255
#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct HSVColor {
    pub h: f64,
    pub s: f64,
    pub v: f64,
}

#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct CroppingConfig {
    pub width: i32,
    pub height: i32,
}

impl CroppingConfig {
    pub fn default() -> Self {
        CroppingConfig {
            width: 500,
            height: 300,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct BallFinderConfig {
    pub mask_blur_size: i32,
    pub alpha: f64,
    pub noise_threshold: f64,
    pub ball_size: i32,
    pub ball_threshold: f64,
}

impl BallFinderConfig {
    pub fn default() -> Self {
        BallFinderConfig {
            mask_blur_size: 11,
            alpha: 0.01,
            noise_threshold: 200.0,
            ball_size: 15,
            ball_threshold: 30.0,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct RodsFinderConfig {
    pub rods_dist: f32,
    pub rods_width: [f32; 4],
    pub foosmen_width: f32,
}

impl RodsFinderConfig {
    pub fn default() -> Self {
        RodsFinderConfig {
            rods_dist: 0.132,
            rods_width: [0.64, 0.39, 0.735, 0.655],
            foosmen_width: 0.005,
        }
    }
}
