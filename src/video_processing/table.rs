use std::error::Error;

use opencv::core::{
    in_range, reduce, Mat, MatTraitConst, Point, Point2f, Rect, Rect2f, Scalar, Size, CV_32F, CV_PI,
};
use opencv::imgproc::{
    canny, cvt_color, get_affine_transform_slice, get_rotation_matrix_2d, hough_lines_p,
    warp_affine, COLOR_BGR2GRAY, COLOR_BGR2HSV, COLOR_GRAY2BGR, COLOR_HSV2BGR,
};
use opencv::types::VectorOfVec4i;

use crate::video_processing::config::{
    ColorsConfig, CroppingConfig, TableOrientationFinderConfig, TablePositionFinderConfig,
};
use crate::video_processing::utils::find_range_with_max_sum;
use crate::video_processing::Feedback;
use opencv::imgproc;

use super::config::Config;
use super::pipeline::PipelineStep;
use super::utils::Avg;

/// Struct that finds the orientation of the table in a given image
///
/// While this could be a function, using a struct lets us reuse the
/// various matrices needed for the computation, so that we don't
/// allocate too much
pub struct TableOrientationFinder {
    gray_src: Mat,
    lines: VectorOfVec4i,
    dst: Mat,
    table_angle: f64,
    avg_table_angle: Avg<f32>,
    last_update_time: f64,
}

impl TableOrientationFinder {
    /// Creates a new instance of the struct, nothing special
    pub(crate) fn new() -> Self {
        TableOrientationFinder {
            gray_src: Mat::default(),
            lines: VectorOfVec4i::new(),
            dst: Mat::default(),
            table_angle: 0.0,
            avg_table_angle: Avg::new(0.05),
            last_update_time: f64::NEG_INFINITY,
        }
    }

    /// The actual useful stuff:
    /// the function takes a `BGR` image as input and return the angle
    /// between the vertical and the rods, in radians, measured counterclockwise
    ///
    /// Be wary that this function is pretty costly (takes around 35ms on my pc)
    ///
    /// Feedbacks:
    /// - canny_and_hough
    pub(crate) fn find_orientation(
        &mut self,
        src: &Mat,
        config: TableOrientationFinderConfig,
        feedback: &Feedback,
    ) -> Result<f64, Box<dyn Error>> {
        cvt_color(src, &mut self.gray_src, COLOR_BGR2GRAY, 0)?;

        canny(
            &self.gray_src,
            &mut self.dst,
            config.canny_threshold1,
            config.canny_threshold2,
            config.canny_aperture_size,
            false,
        )?;

        hough_lines_p(
            &self.dst,
            &mut self.lines,
            5.0,
            config.hough_angle_res * CV_PI / 180.0,
            config.hough_threshold,
            config.hough_min_line_length,
            config.hough_max_line_gap,
        )?;

        if let Some(ref feed) = feedback.canny_and_hough {
            let mut cdst = Mat::default();
            cvt_color(&self.dst, &mut cdst, COLOR_GRAY2BGR, 0)?;
            for l in &self.lines {
                imgproc::line(
                    &mut cdst,
                    Point::new(l[0], l[1]),
                    Point::new(l[2], l[3]),
                    Scalar::new(0.0, 0.0, 255.0, 128.0),
                    1,
                    imgproc::LINE_AA,
                    0,
                )?;
            }
            // radon_transform(&self.dst, &mut cdst, 1.0, 0.0, 180.0, false, true);
            feed.send(cdst)?;
        }

        let mut tans: Vec<f64> = self
            .lines
            .iter()
            .map(|l| ((l[2] - l[0]) as f64) / (l[3] - l[1]) as f64)
            .filter(|&t| -0.7 < t && t < 0.7)
            .filter(|&t| !t.is_nan())
            .collect::<Vec<f64>>();

        for thresh in &[0.3, 0.1, 0.05, 0.02] {
            let avg_tan = tans.iter().sum::<f64>() / tans.len() as f64;

            tans = tans
                .into_iter()
                .filter(|&t| avg_tan - thresh < t && t < avg_tan + thresh)
                .collect::<Vec<f64>>();
        }

        let res = tans.iter().sum::<f64>() / tans.len() as f64;
        Ok(if res.is_nan() {
            *self.avg_table_angle.get() as f64
        } else {
            res
        })
    }
}

impl PipelineStep<Mat, Mat> for TableOrientationFinder {
    fn process(
        &mut self,
        conf: &Config,
        feed: &Feedback,
        timestamp: f64,
        corrected_src: Mat,
    ) -> Result<Mat, Box<dyn Error>> {
        // finding the table orientation costs a lot, and the angle stays pretty much the same,
        // so we only do it every _n_ frames
        if timestamp - self.last_update_time
            > conf
                .table_orientation_finder
                .table_orientation_update_interval
        {
            self.table_angle =
                self.find_orientation(&corrected_src, conf.table_orientation_finder, &feed)?;
            self.last_update_time = timestamp;
        }
        self.avg_table_angle.add(self.table_angle as f32);

        // rotate the image so that the table is horizontal
        let center = Point2f::new(
            corrected_src.cols() as f32 / 2.0,
            corrected_src.rows() as f32 / 2.0,
        );
        let flip = if conf.table_orientation_finder.should_flip_table {
            180.0
        } else {
            0.0
        };
        let rotation = get_rotation_matrix_2d(
            center,
            -*self.avg_table_angle.get() as f64 / CV_PI * 180.0 + flip,
            1.0,
        )?;
        let mut rotated_src = Mat::default();
        warp_affine(
            &corrected_src,
            &mut rotated_src,
            &rotation,
            corrected_src.size()?,
            1,
            0,
            Scalar::new(0.0, 0.0, 0.0, 0.0),
        )?;
        if let Some(ref feed) = feed.rotated {
            feed.send(rotated_src.clone())?;
        }

        Ok(rotated_src)
    }
}

/// Struct that finds the table's position
///
/// Once again, this is a struct and not a function just so that we can cache
/// a few [`Mat`]s
pub struct TablePositionFinder {
    rotated_hsv_src: Mat,
    green_mask: Mat,
    row_sum: Mat,
    col_sum: Mat,
    avg_field_width: Avg<f32>,
}

impl TablePositionFinder {
    /// Creates a new `TablePositionFinder`
    pub fn new() -> Self {
        TablePositionFinder {
            rotated_hsv_src: Mat::default(),
            green_mask: Mat::default(),
            row_sum: Mat::default(),
            col_sum: Mat::default(),
            avg_field_width: Avg::new(0.02),
        }
    }

    /// Finds the table in an already correctly rotated hsv image
    ///
    /// Feedbacks:
    /// - rotated_green
    pub fn find_position(
        &mut self,
        rotated_hsv_src: &Mat,
        colors: ColorsConfig,
        config: TablePositionFinderConfig,
        feedback: &Feedback,
    ) -> Result<Rect2f, Box<dyn Error>> {
        in_range(
            rotated_hsv_src,
            &Scalar::new(
                colors.green_low.h,
                colors.green_low.s,
                colors.green_low.v,
                -1.0,
            ),
            &Scalar::new(
                colors.green_high.h,
                colors.green_high.s,
                colors.green_high.v,
                255.0,
            ),
            &mut self.green_mask,
        )?;

        reduce(&self.green_mask, &mut self.row_sum, 1, 0, CV_32F)?;

        let field_width = if config.enable_fixed_width {
            config.table_width * self.green_mask.rows() as f32
        } else {
            let mut max_y = 0.0;
            let mut max_d = 0.0;
            let mut min_y = 0.0;
            let mut min_d = 0.0;
            for y in 1..self.row_sum.rows() - 1 {
                let d = self.row_sum.at::<f32>(y + 1)? - self.row_sum.at::<f32>(y - 1)?;
                if d > max_d {
                    max_d = d;
                    max_y = y as f32 + 0.5
                        - ((self.row_sum.at::<f32>(y)? - self.row_sum.at::<f32>(y - 1)?) / max_d);
                } else if d < min_d {
                    min_d = d;
                    min_y = y as f32 + 0.5
                        - ((self.row_sum.at::<f32>(y)? - self.row_sum.at::<f32>(y - 1)?) / min_d);
                }
            }

            let field_width = min_y - max_y;
            self.avg_field_width.add(field_width);
            *self.avg_field_width.get()
        };

        let max_y = find_range_with_max_sum(&self.row_sum, field_width);

        reduce(&self.green_mask, &mut self.col_sum, 0, 0, CV_32F)?;

        let field_length = field_width * config.aspect_ratio;
        let max_x = find_range_with_max_sum(&self.col_sum, field_length);
        let pad_x = field_length * config.x_padding;
        let pad_y = field_width as f32 * config.y_padding;
        let dx = field_length * config.x_offset;
        let dy = field_width as f32 * config.y_offset;

        if let Some(ref feed) = feedback.rotated_green {
            let mut green_mask = Mat::default();
            cvt_color(&self.green_mask, &mut green_mask, COLOR_GRAY2BGR, 0)?;
            imgproc::rectangle_def(
                &mut green_mask,
                Rect::new(
                    max_x as i32,
                    max_y as i32,
                    field_length as i32,
                    field_width as i32,
                ),
                Scalar::new(0.0, 0.0, 255.0, 255.0),
            )?;
            feed.send(green_mask)?;
        }

        Ok(Rect2f::new(
            max_x as f32 + dx - pad_x,
            max_y as f32 + dy - pad_y,
            field_length + 2.0 * pad_x,
            field_width as f32 + 2.0 * pad_y,
        ))
    }
}

impl PipelineStep<Mat, Mat> for TablePositionFinder {
    fn process(
        &mut self,
        conf: &Config,
        feed: &Feedback,
        _timestamp: f64,
        rotated_src: Mat,
    ) -> Result<Mat, Box<dyn Error>> {
        // convert the image to the HSV colorspace, which is better(?) for distinguishing colors
        cvt_color(&rotated_src, &mut self.rotated_hsv_src, COLOR_BGR2HSV, 0)?;

        // find the table position
        let table_position = self.find_position(
            &self.rotated_hsv_src.clone(),
            conf.colors,
            conf.table_position_finder,
            &feed,
        )?;

        let mut hsv_table = Mat::default();

        // crop the frame so that it contains only the table
        crop_table(
            &self.rotated_hsv_src,
            table_position,
            &mut hsv_table,
            &conf.cropping,
        )?;

        if let Some(ref feed) = feed.cropped {
            let mut rgb_table = Mat::default();
            cvt_color(&hsv_table, &mut rgb_table, COLOR_HSV2BGR, 0)?;
            feed.send(rgb_table)?;
        }

        Ok(hsv_table)
    }
}

/// Crops a specified rect from an image
pub fn crop_table(
    rotated_hsv_src: &Mat,
    table_position: Rect2f,
    hsv_table: &mut Mat,
    conf: &CroppingConfig,
) -> Result<(), Box<dyn Error>> {
    let (w, h) = (conf.width, conf.height);

    let input_points = vec![
        Point2f::new(table_position.x, table_position.y),
        Point2f::new(table_position.x + table_position.width, table_position.y),
        Point2f::new(table_position.x, table_position.y + table_position.height),
    ];
    let output_points = vec![
        Point2f::new(0.0, 0.0),
        Point2f::new((w - 1) as f32, 0.0),
        Point2f::new(0.0, (h - 1) as f32),
    ];

    let cropping = get_affine_transform_slice(&input_points, &output_points)?;

    warp_affine(
        rotated_hsv_src,
        hsv_table,
        &cropping,
        Size::new(w, h),
        0,
        0,
        Scalar::new(0.0, 0.0, 0.0, 0.0),
    )?;

    Ok(())
}
