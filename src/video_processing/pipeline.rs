use std::{
    error::Error,
    sync::mpsc::{sync_channel, Receiver, SyncSender},
    thread::{self},
    time::{Duration, Instant},
};

use cpu_time::ThreadTime;

use super::{config::Config, Feedback};

/// A step that gets run in a [`Pipeline`]
pub trait PipelineStep<In, Out> {
    /// Processes some data
    fn process(
        &mut self,
        conf: &Config,
        feed: &Feedback,
        timestamp: f64,
        data: In,
    ) -> Result<Out, Box<dyn Error>>;
}

impl<In, Out, F> PipelineStep<In, Out> for F
where
    F: Fn(&Config, &Feedback, f64, In) -> Result<Out, Box<dyn Error>>,
{
    fn process(
        &mut self,
        conf: &Config,
        feed: &Feedback,
        timestamp: f64,
        data: In,
    ) -> Result<Out, Box<dyn Error>> {
        self(conf, feed, timestamp, data)
    }
}

const BUFFER_SIZE: usize = 3;

/// Processes some data by running a sequence of steps.
/// Each step is run in its own thread, so that multiple data can be processed concurrently
pub struct Pipeline<In> {
    input_channel: SyncSender<(PipelineInfo, Config, Feedback, f64, In)>,
}

/// Build a [`Pipeline`], by adding the [`PipelineStep`]s one by one
pub struct PipelineBuilder<In, Out> {
    input_channel: SyncSender<(PipelineInfo, Config, Feedback, f64, In)>,
    output_channel: Receiver<(PipelineInfo, Config, Feedback, f64, Out)>,
}

impl<In: Send, Out: Send + 'static> PipelineBuilder<In, Out> {
    pub fn new<T>() -> PipelineBuilder<T, T> {
        let (input_channel, output_channel) = sync_channel(BUFFER_SIZE);
        PipelineBuilder {
            input_channel,
            output_channel,
        }
    }

    /// Adds a new step, creating a thread for it
    pub fn add_step<NewOut: Send + 'static, Ps>(
        self,
        name: &'static str,
        mut step: Ps,
    ) -> PipelineBuilder<In, NewOut>
    where
        Ps: PipelineStep<Out, NewOut> + Send + 'static,
    {
        let (input_channel, output_channel) = sync_channel(BUFFER_SIZE);
        let _step_thread = thread::spawn(move || {
            while let Ok((mut info, conf, feed, timestamp, data)) = self.output_channel.recv() {
                let start = ThreadTime::now();
                let res = step.process(&conf, &feed, timestamp, data);
                let time = start.elapsed();
                info.timings.push((name, time));
                match res {
                    Ok(res) => input_channel
                        .send((info, conf, feed, timestamp, res))
                        .unwrap(),
                    Err(e) => eprintln!("{}", e),
                }
            }
            eprintln!("Pipeline Step Quitting");
        });
        PipelineBuilder {
            input_channel: self.input_channel,
            output_channel,
        }
    }

    pub fn start(self) -> Pipeline<In> {
        let PipelineBuilder {
            input_channel,
            output_channel,
        } = self;
        let _step_thread = thread::spawn(move || {
            while let Ok((mut info, _, feed, _, _)) = output_channel.recv() {
                info.duration = info.start_time.elapsed();
                if let Some(feed) = feed.pipeline_info {
                    feed.send(info).unwrap();
                }
            }
        });
        Pipeline { input_channel }
    }
}

impl<In> Pipeline<In> {
    /// Sends some data into the pipeline for processing
    pub fn process(&mut self, conf: Config, feed: Feedback, timestamp: f64, data: In) {
        self.input_channel
            .send((PipelineInfo::new(), conf, feed, timestamp, data))
            .unwrap();
    }
}

pub struct PipelineInfo {
    pub start_time: Instant,
    pub timings: Vec<(&'static str, Duration)>,
    pub duration: Duration,
}

impl PipelineInfo {
    fn new() -> Self {
        Self {
            start_time: Instant::now(),
            duration: Duration::ZERO,
            timings: vec![],
        }
    }
}
