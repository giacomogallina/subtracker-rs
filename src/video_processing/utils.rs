use opencv::core::{Mat, MatTraitConst};
use std::ops::{AddAssign, Mul, MulAssign};

/// Given a 1-dimensional opencv [`Mat`] of type [`CV_32F`][opencv::core::CV_32F] (basically a vector) and a
/// `range_width`, finds the index of the left extreme of the range of width `range_width`
/// with maximum sum
pub fn find_range_with_max_sum(vec: &Mat, range_width: f32) -> f32 {
    let n = std::cmp::max(vec.cols(), vec.rows());
    let mut max_pos = 0.0;
    let mut int_sum = 0.0;
    let w = range_width.floor() as i32;
    let f = range_width - w as f32;
    for i in 0..=w.max(n - 1) {
        int_sum += vec.at::<f32>(i as i32).unwrap();
    }

    let mut max_s = 0.0;
    for i in 0..(n - 2 - w) {
        let v_i = vec.at::<f32>(i).unwrap();
        let v_i1 = vec.at::<f32>(i + 1).unwrap();
        let v_iw = vec.at::<f32>(i + w).unwrap();
        let v_iw1 = vec.at::<f32>(i + w + 1).unwrap();
        let v_iw2 = vec.at::<f32>(i + w + 2).unwrap();
        let v_iwf = v_iw * (1.0 - f) + v_iw1 * f;
        let v_iw1f = v_iw1 * (1.0 - f) + v_iw2 * f;
        let v_i1_f = v_i1 * (1.0 - f) + v_i * f;
        let sum = int_sum - v_i - 0.5 * (v_i1 + v_iw) + f * 0.5 * (v_iw + v_iwf);
        let i_0_to_1_f = (1.0 - f) * 0.5 * (v_i + v_i1_f);
        let i_1_f_to_1 = f * 0.5 * (v_i1_f + v_i1);
        let i_1_to_2_f = (1.0 - f) * 0.5 * (v_iwf + v_iw1);
        let i_2_f_to_2 = f * 0.5 * (v_iw1 + v_iw1f);
        let i_0 = i_0_to_1_f + i_1_f_to_1;
        let i_1_f = i_1_f_to_1 + i_1_to_2_f;
        let i_1 = i_1_to_2_f + i_2_f_to_2;
        let s1 = ((v_i - v_iwf) / (v_i - v_i1_f - v_iwf + v_iw1)).clamp(0.0, 1.0);
        let s2 = ((v_i1_f - v_iw1) / (v_i1_f - v_i1 - v_iw1 + v_iw1f)).clamp(0.0, 1.0);
        let i_s1 = i_0 - (1.0 - f) * (s1 * (v_i + 0.5 * s1 * (v_i1_f - v_i)))
            + (1.0 - f) * (s1 * (v_iwf + 0.5 * s1 * (v_iw1 - v_iwf)));
        let i_s2 = i_1_f - f * (s2 * (v_i1_f + 0.5 * s2 * (v_i1 - v_i1_f)))
            + f * (s2 * (v_iw1 + 0.5 * s2 * (v_iw1f - v_iw1)));

        if sum + i_0 > max_s {
            max_s = sum + i_0;
            max_pos = i as f32;
        }
        if sum + i_s1 > max_s {
            max_s = sum + i_s1;
            max_pos = i as f32 + (1.0 - f) * s1;
        }
        if sum + i_1_f > max_s {
            max_s = sum + i_1_f;
            max_pos = i as f32 + (1.0 - f);
        }
        if sum + i_s2 > max_s {
            max_s = sum + i_s2;
            max_pos = i as f32 + (1.0 - f) + f * s2;
        }
        if sum + i_1 > max_s {
            max_s = sum + i_1;
            max_pos = i as f32 + 1.0;
        }

        if (i + w + 1) < n {
            int_sum += v_iw1 - v_i;
        }
    }

    max_pos
}

pub fn matmul(a: [[f32; 3]; 3], b: [[f32; 3]; 3]) -> [[f32; 3]; 3] {
    let mut res = [[0.0; 3]; 3];
    for i in 0..3 {
        for j in 0..3 {
            res[i][j] = (0..3).map(|k| a[i][k] * b[k][j]).sum();
        }
    }
    return res;
}

pub struct Avg<T> {
    val: T,
    initialized: bool,
    alpha: f32,
}

impl<T> Avg<T>
where
    T: AddAssign<T> + MulAssign<f32> + Mul<f32, Output = T> + Default,
{
    pub fn new(alpha: f32) -> Self {
        Self {
            val: T::default(),
            initialized: false,
            alpha,
        }
    }

    pub fn add(&mut self, new: T) {
        if self.initialized {
            self.val *= 1.0 - self.alpha;
            self.val += new * self.alpha;
        } else {
            self.val = new;
            self.initialized = true;
        }
    }

    pub fn get(&self) -> &T {
        &self.val
    }
}
