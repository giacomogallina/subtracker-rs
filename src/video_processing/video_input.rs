use crate::video_processing::{config, utils};
use opencv::{
    core::{Mat, Ptr, Scalar},
    hub_prelude::MatTraitConst,
    imgproc,
    prelude::WhiteBalancerTrait,
    videoio::{prelude::VideoCaptureTrait, VideoCapture},
    xphoto::{create_grayworld_wb, GrayworldWB, GrayworldWBTrait},
};
use std::error::Error;
use std::time::Instant;

use super::pipeline::PipelineStep;

/// Struct that reads timestamped frames from a rtp stream
///
/// Implements the [Iterator] Trait, so that you can iterate over the frames in a for loop:
/// ```
/// for (timestamp, frame) in image_stream {
///     // do stuff
/// }
/// ```
/// where frame is a `Mat`
pub struct VideoStream {
    cap: VideoCapture,
}

impl VideoStream {
    pub fn new(connection: &config::ConnectionConfig) -> Result<Self, Box<dyn Error>> {
        let filename = match connection.connection_type {
            config::ConnectionType::RTSP => format!(
                "rtsp://{}:{}/subtracker",
                connection.rtsp_ip, connection.rtsp_port
            ),
            config::ConnectionType::Manual => connection.manual_url.clone(),
        };
        let cap = VideoCapture::from_file(&filename, 0)?;

        Ok(Self { cap })
    }
}

impl Iterator for VideoStream {
    type Item = (Instant, Mat);

    fn next(&mut self) -> Option<Self::Item> {
        let mut frame = Mat::default();

        if let Ok(true) = self.cap.read(&mut frame) {
            Some((Instant::now(), frame))
        } else {
            None
        }
    }
}

pub fn correct_camera_angle(conf: config::CameraConfig, src: Mat) -> Result<Mat, Box<dyn Error>> {
    // the camera probably won't be pointing perpendicular to the table, so we correct
    // with a projective transformation
    let (a, b) = (conf.x_angle, conf.y_angle);
    if (a, b) == (0.0, 0.0) {
        return Ok(src);
    }

    let (w, h) = (src.cols() as f32, src.rows() as f32);
    let projectivity = Mat::from_slice_2d(&utils::matmul(
        utils::matmul(
            [
                [w / 2.0, 0.0, w / 2.0],
                [0.0, h / 2.0, h / 2.0], // restore the image to [0, w] × [0, h]
                [0.0, 0.0, 1.0],
            ],
            [
                [a.cos(), 0.0, a.sin()],
                [-a.sin() * b.sin(), b.cos(), a.cos() * b.sin()], // projective transformation
                [-a.sin() * b.cos(), -b.sin(), a.cos() * b.cos()], // it's not very symmetric, but it's not an issue for small angles
            ],
        ),
        [
            [2.0 / w, 0.0, -1.0],
            [0.0, 2.0 / h, -1.0], // center the image in [-1, 1]²
            [0.0, 0.0, 1.0],
        ],
    ))?;
    let mut corrected_src = Mat::default();
    imgproc::warp_perspective(
        &src,
        &mut corrected_src,
        &projectivity,
        src.size()?,
        imgproc::WARP_INVERSE_MAP | imgproc::INTER_LINEAR,
        opencv::core::BORDER_CONSTANT,
        Scalar::new(0.0, 0.0, 0.0, 0.0),
    )?;

    Ok(corrected_src)
}

pub struct WhiteBalancer {
    wb: Ptr<GrayworldWB>,
}

impl WhiteBalancer {
    pub fn new() -> Self {
        // let mut wb = unsafe {
        //     GrayworldWB::from_raw(create_grayworld_wb().unwrap().as_raw_mut_GrayworldWB())
        // };
        let mut wb = create_grayworld_wb().unwrap();
        wb.set_saturation_threshold(0.25).unwrap();
        Self { wb }
    }
}

unsafe impl Send for WhiteBalancer {}

impl PipelineStep<Mat, Mat> for WhiteBalancer {
    fn process(
        &mut self,
        conf: &config::Config,
        _feed: &super::Feedback,
        _timestamp: f64,
        src: Mat,
    ) -> Result<Mat, Box<dyn Error>> {
        Ok(if conf.camera.auto_white_balance {
            let mut balanced_src = Mat::default();
            self.wb.balance_white(&src, &mut balanced_src)?;
            balanced_src
        } else {
            src
        })
    }
}
