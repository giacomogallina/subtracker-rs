use std::error::Error;

use crate::video_processing::utils::find_range_with_max_sum;
use crate::video_processing::Feedback;
use opencv::core::{
    bitwise_or, in_range, merge, reduce, Mat, MatTraitConst, Range, Scalar, CV_32F,
};
use opencv::types::VectorOfMat;
use std::cmp::{max, min};

use super::config::Config;
use super::pipeline::PipelineStep;

/// The type if the `i`th rod from the left: 0 is the goalkeeper and 3 is the forward
pub const RODS_TYPE: [usize; 8] = [0, 1, 3, 2, 2, 3, 1, 0];
/// The team of the `i`th rod from the left: 0 is red team and 1 is blue team
pub const RODS_TEAM: [usize; 8] = [0, 0, 1, 0, 1, 0, 1, 1];
/// How many foosmen the `i`th type of rod has (0 is the goalkeeper and 3 is the forward)
pub const RODS_MEN: [i32; 4] = [1, 2, 5, 3];

/// Generates the red-blue mask used by both the `BallFinder` and the `RodsFinder`
pub struct RedBlueMaskGenerator {
    blue_mask: Mat,
    red_mask_low: Mat,
    red_mask_high: Mat,
    red_mask: Mat,
    // every field past here is only used for the color masks feedback
    green_mask: Mat,
    white_mask: Mat,
    green_white_mask: Mat,
    red_white_mask: Mat,
    blue_white_mask: Mat,
    colors: Mat,
}

impl RedBlueMaskGenerator {
    /// Creates a new `RedBlueMaskGenerator`
    pub fn new() -> Self {
        RedBlueMaskGenerator {
            blue_mask: Mat::default(),
            red_mask_low: Mat::default(),
            red_mask_high: Mat::default(),
            red_mask: Mat::default(),
            green_mask: Mat::default(),
            white_mask: Mat::default(),
            green_white_mask: Mat::default(),
            red_white_mask: Mat::default(),
            blue_white_mask: Mat::default(),
            colors: Mat::default(),
        }
    }
}

impl PipelineStep<Mat, (Mat, Mat)> for RedBlueMaskGenerator {
    /// Creates the red-blue mask
    ///
    /// Feedbacks:
    /// - colors
    fn process(
        &mut self,
        conf: &Config,
        feed: &Feedback,
        _timestamp: f64,
        hsv_table: Mat,
    ) -> Result<(Mat, Mat), Box<dyn Error>> {
        let colors = conf.colors;

        in_range(
            &hsv_table,
            &Scalar::new(
                colors.blue_low.h,
                colors.blue_low.s,
                colors.blue_low.v,
                -1.0,
            ),
            &Scalar::new(
                colors.blue_high.h,
                colors.blue_high.s,
                colors.blue_high.v,
                255.0,
            ),
            &mut self.blue_mask,
        )?;

        in_range(
            &hsv_table,
            &Scalar::new(colors.red_low.h, colors.blue_low.s, colors.red_low.v, -1.0),
            &Scalar::new(181.0, colors.red_high.s, colors.red_high.v, 255.0),
            &mut self.red_mask_high,
        )?;
        in_range(
            &hsv_table,
            &Scalar::new(-1.0, colors.red_low.s, colors.red_low.v, -1.0),
            &Scalar::new(
                colors.red_high.h,
                colors.red_high.s,
                colors.red_high.v,
                255.0,
            ),
            &mut self.red_mask_low,
        )?;
        bitwise_or(
            &self.red_mask_high,
            &self.red_mask_low,
            &mut self.red_mask,
            &Mat::default(),
        )?;

        let mut red_blue_mask = Mat::default();

        bitwise_or(
            &self.blue_mask,
            &self.red_mask,
            &mut red_blue_mask,
            &Mat::default(),
        )?;

        if let Some(ref feed) = feed.colors {
            in_range(
                &hsv_table,
                &Scalar::new(
                    colors.green_low.h,
                    colors.green_low.s,
                    colors.green_low.v,
                    -1.0,
                ),
                &Scalar::new(
                    colors.green_high.h,
                    colors.green_high.s,
                    colors.green_high.v,
                    255.0,
                ),
                &mut self.green_mask,
            )?;
            in_range(
                &hsv_table,
                &Scalar::new(
                    colors.white_low.h,
                    colors.white_low.s,
                    colors.white_low.v,
                    -1.0,
                ),
                &Scalar::new(
                    colors.white_high.h,
                    colors.white_high.s,
                    colors.white_high.v,
                    255.0,
                ),
                &mut self.white_mask,
            )?;
            bitwise_or(
                &self.red_mask,
                &self.white_mask,
                &mut self.red_white_mask,
                &Mat::default(),
            )?;
            bitwise_or(
                &self.blue_mask,
                &self.white_mask,
                &mut self.blue_white_mask,
                &Mat::default(),
            )?;
            bitwise_or(
                &self.green_mask,
                &self.white_mask,
                &mut self.green_white_mask,
                &Mat::default(),
            )?;

            let channels = VectorOfMat::from_iter(vec![
                self.blue_white_mask.clone(),
                self.green_white_mask.clone(),
                self.red_white_mask.clone(),
            ]);

            merge(&channels, &mut self.colors)?;

            feed.send(self.colors.clone())?;
        }

        Ok((hsv_table, red_blue_mask))
    }
}

/// Struct that finds the rods' positions
pub struct RodsFinder {
    strip_row_sum: Mat,
    strip_col_sum: Mat,
}

impl RodsFinder {
    /// Creates a new RodsFinder
    pub fn new() -> Self {
        RodsFinder {
            strip_row_sum: Mat::default(),
            strip_col_sum: Mat::default(),
        }
    }
}

impl PipelineStep<(Mat, Mat), (Mat, Mat, ([f32; 8], [f32; 8]))> for RodsFinder {
    /// Finds the rods' positions and rotation
    ///
    /// Returns an couple of arrays `(p, r)` where `p[i]` is the downward displacement of the center of
    /// the `i`th leftmost rod relative to its central position, expressed in
    /// multiples of the table width (which is also the image height), and `r[i]` is the rotation of the `i`th
    /// rod in radians, where 0 means the rod is vertical and a positive value means the foosmen' feet are pointing
    /// right
    ///
    /// This way -0.5 < `p[i]` < 0.5 and -π/2 < `r[i]` < π/2
    fn process(
        &mut self,
        conf: &Config,
        _feed: &Feedback,
        _timestamp: f64,
        (hsv_table, red_blue_mask): (Mat, Mat),
    ) -> Result<(Mat, Mat, ([f32; 8], [f32; 8])), Box<dyn Error>> {
        let conf = conf.rods_finder;

        let (w, h) = (red_blue_mask.cols(), red_blue_mask.rows());
        // distance between consecutive rods, in pixels
        let d = conf.rods_dist * w as f32;
        // radius that we consider around rods, in pixels
        let r = d / 2.0 * 0.9;
        // x coordinate of the first rod
        let offset = (w as f32 - 7.0 * d) / 2.0;

        let mut rods_pos = [0.0; 8];
        let mut rods_rot = [0.0; 8];

        for i in 0..8_usize {
            let left = max((offset + (i as f32) * d - r).ceil() as i32, 0);
            let center = offset + (i as f32) * d;
            let right = min((offset + (i as f32) * d + r).floor() as i32, w);

            // cut a strip with radius r around the ith rod
            let strip = red_blue_mask.col_range(&Range::new(left, right)?)?;

            // sum along rows and columns
            reduce(&strip, &mut self.strip_row_sum, 1, 0, CV_32F)?;
            reduce(&strip, &mut self.strip_col_sum, 0, 0, CV_32F)?;
            // let strip_row_sum = self.strip_row_sum.to_vec();
            // let strip_col_sum = self.strip_col_sum.to_vec();

            // find rods position
            let rod_width = (h as f32 * conf.rods_width[RODS_TYPE[i]]).round();
            let y = find_range_with_max_sum(&self.strip_row_sum, rod_width);
            rods_pos[i] = (y - (h as f32 - rod_width) / 2.0) / (h as f32);

            // find rods rotation
            let thresh = 255.0 * w as f32 * conf.foosmen_width * RODS_MEN[RODS_TYPE[i]] as f32;
            let left_protrusion = (0..(center.round() as i32 - left))
                .rev()
                .take_while(|c| *self.strip_col_sum.at::<f32>(*c).unwrap() >= thresh)
                .count() as f32;
            let right_protrusion = ((center.round() as i32 - left)..(right - left))
                .take_while(|c| *self.strip_col_sum.at::<f32>(*c).unwrap() >= thresh)
                .count() as f32;
            let (sign, head, feet) = if left_protrusion < right_protrusion {
                (1.0, left_protrusion, right_protrusion)
            } else {
                (-1.0, right_protrusion, left_protrusion)
            };
            rods_rot[i] = (sign * (feet - head) / (r - head)).clamp(-1.0, 1.0).asin();
        }

        Ok((hsv_table, red_blue_mask, (rods_pos, rods_rot)))
    }
}
