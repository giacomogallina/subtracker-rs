use std::collections::VecDeque;
use std::error::Error;

use crate::video_processing::Feedback;
use opencv::core::{
    add_weighted, bitwise_and, in_range, min_max_loc, Mat, MatTraitConst, Point, Scalar, Size,
    CV_32FC3, CV_8UC3,
};
use opencv::imgproc::{self, blur, circle, line, threshold};

use super::config::Config;
use super::pipeline::PipelineStep;
use super::rods::{RODS_MEN, RODS_TEAM, RODS_TYPE};

/// Struct that finds the ball
///
/// Could have been a function, but by using a struct we can cache some [`Mat`]s
/// instead of allocating them every time
pub struct BallFinder {
    blurred_red_blue_mask: Mat,
    avg_background: Mat,
    avg_background_u8: Mat,
    hsv_table_f32: Mat,
    white_mask: Mat,
    avg_background_green_mask: Mat,
    non_avg_white_mask: Mat,
    blurred_non_avg_white_mask: Mat,
    non_avg_white_denoised_mask: Mat,
    blurred_non_avg_white_denoised_mask: Mat,
    last_ball_positions: VecDeque<(f64, Option<Point>)>,
}

impl BallFinder {
    /// Creates a new `BallFinder`
    pub fn new() -> Self {
        BallFinder {
            blurred_red_blue_mask: Mat::default(),
            avg_background: Mat::default(),
            avg_background_u8: Mat::default(),
            hsv_table_f32: Mat::default(),
            white_mask: Mat::default(),
            avg_background_green_mask: Mat::default(),
            non_avg_white_mask: Mat::default(),
            blurred_non_avg_white_mask: Mat::default(),
            non_avg_white_denoised_mask: Mat::default(),
            blurred_non_avg_white_denoised_mask: Mat::default(),
            last_ball_positions: VecDeque::new(),
        }
    }
}

impl PipelineStep<(Mat, Mat, ([f32; 8], [f32; 8])), (Mat, Mat, ([f32; 8], [f32; 8]), Option<Point>)>
    for BallFinder
{
    /// Finds the ball
    ///
    /// Needs:
    /// - current table image in HSV
    /// - a mask of the pixels that are either red or blue
    /// - some configs
    ///
    /// Returns the ball position in the table image coordinates
    ///
    /// Feedbacks:
    /// - avg_background
    /// - non_avg_white
    /// - non_avg_white_denoised
    fn process(
        &mut self,
        config: &Config,
        feedback: &Feedback,
        timestamp: f64,
        (hsv_table, red_blue_mask, rods): (Mat, Mat, ([f32; 8], [f32; 8])),
    ) -> Result<(Mat, Mat, ([f32; 8], [f32; 8]), Option<Point>), Box<dyn Error>> {
        blur(
            &red_blue_mask,
            &mut self.blurred_red_blue_mask,
            Size::new(
                config.ball_finder.mask_blur_size,
                config.ball_finder.mask_blur_size,
            ),
            Point::new(-1, -1),
            0,
        )?;

        let alpha = config.ball_finder.alpha;
        if self.avg_background.size()? == Size::new(0, 0) {
            hsv_table.convert_to(&mut self.avg_background, CV_32FC3, 1.0, 0.0)?;
        } else {
            let old_avg_background = self.avg_background.clone();
            hsv_table.convert_to(&mut self.hsv_table_f32, CV_32FC3, 1.0, 0.0)?;
            old_avg_background
                .copy_to_masked(&mut self.hsv_table_f32, &self.blurred_red_blue_mask)?;
            add_weighted(
                &old_avg_background,
                1.0 - alpha,
                &self.hsv_table_f32,
                alpha,
                0.0,
                &mut self.avg_background,
                CV_32FC3,
            )?;
        }

        if let Some(ref feed) = feedback.avg_background {
            self.avg_background
                .convert_to(&mut self.avg_background_u8, CV_8UC3, 1.0, 0.0)?;
            feed.send(self.avg_background_u8.clone())?;
        }

        in_range(
            &hsv_table,
            &Scalar::new(
                config.colors.white_low.h,
                config.colors.white_low.s,
                config.colors.white_low.v,
                -1.0,
            ),
            &Scalar::new(
                config.colors.white_high.h,
                config.colors.white_high.s,
                config.colors.white_high.v,
                255.0,
            ),
            &mut self.white_mask,
        )?;
        in_range(
            &self.avg_background,
            &Scalar::new(
                config.colors.green_low.h,
                config.colors.green_low.s,
                config.colors.green_low.v,
                -1.0,
            ),
            &Scalar::new(
                config.colors.green_high.h,
                config.colors.green_high.s,
                config.colors.green_high.v,
                255.0,
            ),
            &mut self.avg_background_green_mask,
        )?;

        bitwise_and(
            &self.white_mask,
            &self.avg_background_green_mask,
            &mut self.non_avg_white_mask,
            &Mat::default(),
        )?;

        if let Some(ref feed) = feedback.non_avg_white {
            feed.send(self.non_avg_white_mask.clone())?;
        }

        // remove a bit of noise
        blur(
            &self.non_avg_white_mask,
            &mut self.blurred_non_avg_white_mask,
            Size::new(3, 3),
            Point::new(-1, -1),
            0,
        )?;
        threshold(
            &self.blurred_non_avg_white_mask,
            &mut self.non_avg_white_denoised_mask,
            config.ball_finder.noise_threshold,
            255.0,
            0,
        )?;

        if let Some(ref feed) = feedback.non_avg_white_denoised {
            feed.send(self.non_avg_white_denoised_mask.clone())?;
        }

        // to find the ball, we search for the square of size `config.ball_size` with maximum sum,
        // and to do that, we blur the image and search for the brightest pixel
        // definitely not the fastest way to do that, but the easiest to write
        blur(
            &self.non_avg_white_denoised_mask,
            &mut self.blurred_non_avg_white_denoised_mask,
            Size::new(config.ball_finder.ball_size, config.ball_finder.ball_size),
            Point::new(-1, -1),
            0,
        )?;
        // let mut min_v = 0.0;
        let mut max_v = 0.0;
        // let mut min_pos = Point::default();
        let mut max_pos = Default::default();
        min_max_loc(
            &self.blurred_non_avg_white_denoised_mask,
            None,
            // &mut min_v,
            Some(&mut max_v),
            None,
            // &mut min_pos,
            Some(&mut max_pos),
            &Mat::default(),
        )?;

        let guessed_ball_pos = if let Some(&(t_1, Some(x_1))) = self.last_ball_positions.get(0) {
            if let Some(&(t_2, Some(x_2))) = self.last_ball_positions.get(0) {
                let x_0 = x_1.to::<f64>().unwrap()
                    + (x_1 - x_2).to::<f64>().unwrap() / (t_1 - t_2) * (timestamp - t_1);
                Some(Point::new(x_0.x as i32, x_0.y as i32))
            } else {
                Some(x_1)
            }
        } else {
            None
        };

        let closeness_bonus = if let Some(g) = guessed_ball_pos {
            (-((max_pos - g).norm() * 2.0 / config.ball_finder.ball_size as f64).powi(2)).exp()
        } else {
            0.0
        };

        let ball_pos = if max_v + closeness_bonus
            > config.ball_finder.ball_threshold * (1.0 - 0.5 * closeness_bonus)
        {
            Some(max_pos)
        } else {
            None
        };

        self.last_ball_positions.push_front((timestamp, ball_pos));
        while self.last_ball_positions.len() > 10 {
            self.last_ball_positions.pop_back();
        }

        Ok((hsv_table, red_blue_mask, rods, ball_pos))
    }
}

#[derive(Default)]
pub struct AnalysisDrawer {}

impl
    PipelineStep<
        (Mat, Mat, ([f32; 8], [f32; 8]), Option<Point>),
        (([f32; 8], [f32; 8]), Option<Point>),
    > for AnalysisDrawer
{
    /// if the GUI wants it, we send it an image with the ball circled and an
    /// overlay that indicates the rods positions
    fn process(
        &mut self,
        conf: &Config,
        feed: &Feedback,
        _timestamp: f64,
        (mut hsv_table, _red_blue_mask, (rods_pos, rods_rot), ball_position): (
            Mat,
            Mat,
            ([f32; 8], [f32; 8]),
            Option<Point>,
        ),
    ) -> Result<(([f32; 8], [f32; 8]), Option<Point>), Box<dyn Error>> {
        let (w, h) = (conf.cropping.width as f32, conf.cropping.height as f32);

        if let Some(feed) = &feed.ball_and_rods_pos {
            // circle the ball
            if let Some(pos) = ball_position {
                circle(
                    &mut hsv_table,
                    pos,
                    conf.ball_finder.ball_size / 2 + 3,
                    Scalar::new(255.0, 0.0, 0.0, 150.0),
                    3,
                    0,
                    0,
                )?;
            }
            for (i, (rp, rr)) in rods_pos.iter().zip(rods_rot.iter()).enumerate() {
                let x = w * (0.5 + conf.rods_finder.rods_dist * (i as f32 - 3.5));
                let rw = conf.rods_finder.rods_width[RODS_TYPE[i]];
                let rod_color = [
                    Scalar::new(96.0, 128.0, 255.0, 255.0),
                    Scalar::new(255.0, 128.0, 64.0, 255.0),
                ][RODS_TEAM[i]];
                // draw over the rods
                line(
                    &mut hsv_table,
                    Point::new(x as i32, (h * (0.5 - 0.5 * rw + rp)) as i32),
                    Point::new(x as i32, (h * (0.5 + 0.5 * rw + rp)) as i32),
                    rod_color,
                    5,
                    imgproc::LINE_AA,
                    0,
                )?;
                // draw the inclined foosmen
                let men_n = RODS_MEN[RODS_TYPE[i]];
                for j in 0..men_n {
                    let offset = if men_n == 1 { 0.0 } else { -rw * 0.5 };
                    let dy = rw / (men_n - 1).max(1) as f32;
                    line(
                        &mut hsv_table,
                        Point::new(x as i32, (h * (0.5 + rp + offset + j as f32 * dy)) as i32),
                        Point::new(
                            (x + rr.sin() * w * 0.5 * conf.rods_finder.rods_dist) as i32,
                            (h * (0.5 + rp + offset + j as f32 * dy)) as i32,
                        ),
                        rod_color,
                        5,
                        imgproc::LINE_AA,
                        0,
                    )?;
                }
            }
            feed.send(hsv_table.clone())?;
        }

        Ok(((rods_pos, rods_rot), ball_position))
    }
}
