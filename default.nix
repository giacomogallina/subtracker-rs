with import <nixpkgs> {};
let buildInputs = [
        pkg-config
        fontconfig
        opencv
        clang
        xorg.libXcursor
        xorg.libxcb
        xorg.libXrandr
        xorg.libXi
        xorg.libX11
        libxkbcommon
        libGL
        libGLU
        wayland
        gnome.zenity
        ];
in stdenv.mkDerivation {
    name = "build-environment"; # Probably put a more meaningful name here
    buildInputs = buildInputs;
    nativeBuildInputs = [ clang ];
    LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";
    shellHook = ''
        export BINDGEN_EXTRA_CLANG_ARGS="$(< ${stdenv.cc}/nix-support/libc-crt1-cflags) \
          $(< ${stdenv.cc}/nix-support/libc-cflags) \
          $(< ${stdenv.cc}/nix-support/cc-cflags) \
          $(< ${stdenv.cc}/nix-support/libcxx-cxxflags) \
          ${
            lib.optionalString stdenv.cc.isClang
            "-idirafter ${stdenv.cc.cc}/lib/clang/${
              lib.getVersion stdenv.cc.cc
            }/include"
          } \
          ${
            lib.optionalString stdenv.cc.isGNU
            "-isystem ${stdenv.cc.cc}/include/c++/${
              lib.getVersion stdenv.cc.cc
            } -isystem ${stdenv.cc.cc}/include/c++/${
              lib.getVersion stdenv.cc.cc
            }/${stdenv.hostPlatform.config} -idirafter ${stdenv.cc.cc}/lib/gcc/${stdenv.hostPlatform.config}/${
              lib.getVersion stdenv.cc.cc
            }/include"
          } \
        "
        # export LD_LIBRARY_PATH=/run/opengl-driver/lib/:${lib.makeLibraryPath ([libGL libGLU xorg.libXcursor])}
    '';
    LD_LIBRARY_PATH="${lib.makeLibraryPath buildInputs}";
}
